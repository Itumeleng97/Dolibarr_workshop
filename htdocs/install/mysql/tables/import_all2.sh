#!/bin/bash

# MySQL connection settings
USER="mohlala"
PASSWORD="Im@466641201"
DATABASE="Dolibarr_db"

# Loop through SQL files in the directory and import only files ending with ".key.sql"
for FILE in *.key.sql
do
    echo "Importing $FILE..."
    mysql -u $USER -p$PASSWORD $DATABASE < $FILE
    echo "Imported $FILE."
done
