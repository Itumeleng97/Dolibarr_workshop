#!/bin/bash

# MySQL connection settings
USER="mohlala"
PASSWORD="Im@466641201"
DATABASE="Dolibarr_db"

# Loop through SQL files in the directory and import them
for FILE in *.sql
do
    # Check if the file name contains the word "Key"
    if [[ $FILE != *"Key"* ]]; then
        echo "Importing $FILE..."
        mysql -u $USER -p$PASSWORD $DATABASE < $FILE
        echo "Imported $FILE."
    else
        echo "Skipping $FILE as it contains 'Key'."
    fi
done
