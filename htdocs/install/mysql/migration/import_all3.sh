#!/bin/bash

# MySQL connection settings
USER="mohlala"
PASSWORD="Im@466641201"
DATABASE="Dolibarr_db"

# Loop through SQL files in the directory and import only files ending with ".sql"
for FILE in *.sql
do
    echo "Importing $FILE..."
    mysql -u $USER -p$PASSWORD $DATABASE < $FILE
    echo "Imported $FILE."
done
