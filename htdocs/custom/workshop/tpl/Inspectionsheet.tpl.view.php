<!DOCTYPE html>
<html>
<head>
  <title>inspectionsheet_card</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>


<body>
  <!-- Your PHP code and HTML content here -->
</body>
</html>
<?php



		print '	<div class="container" id="inspectionlines-tabs" style="display:flex;">'	;
		print '<ul class="nav nav-tabs" style="border-bottom: 2px solid #007bff;">';
		print '	<li class="nav-item"><a class="nav-link active" id="main-tab" data-toggle="tab" href="#main-view" style="border: 1px solid #007bff; border-bottom: none;">Main-Version</a></li>';			
		print '<li class="nav-item"><a class="nav-link" id="reducted-tab" data-toggle="tab" href="#reducted-view" style="border: 1px solid #007bff; border-bottom: none;">Reducted version</a></li>';
		print '	<li class="nav-item"><a class="nav-link" id="summarised-tab" data-toggle="tab" href="#summarised-view" style="border: 1px solid #007bff; border-bottom: none;">Summarised version</a></li>';	
		print '	</ul>';		
		print ' <hr>';

		print '</div>';



          print '<div class="tab-content">';

          print '<div class="tab-pane fade show active" id="main-view">';
          print '<p>Main Version content goes here</p>';

          
		$header1 = "<br>
    <h2   style='margin-bottom: 10px; color: white; background-color: navy; padding: 10px;'>Header Information</h4>
    <div style='display: flex; flex-wrap: wrap;'>
      <div style='flex: 1; text-align: right; margin-right: 20px;'>
        <h5 style='margin-bottom: 5px;'><strong style='color: navy;'>Date Created  :</strong> {$dateCreated}</h5>
      </div>
      <div style='flex: 1; margin-left: 20px;'>
        <h5 style='margin-bottom: 5px;'><strong style='color: navy;'>Inspection Name  :</strong> {$inspectionname}</h5>
      </div>
      <div style='flex: 1; margin-left: 20px;'>
        <h5 style='margin-bottom: 5px;'><strong style='color: navy;'>Description  :</strong> {$description}</h5>
      </div>
    </div>
  ";




// Start the header information
$header2 = "<div style='display: flex; flex-wrap: wrap;  margin-left: 10px;'>";

// Loop through each header in the array
foreach ($inspectionheaders as $headerInfo) {
// Add the label and value to the header

$header2 .= "
        <div style='margin-bottom: 20px;  margin-left: 20px; padding: 10px; border: 1px solid navy; display: flex; justify-content: center;'>
          <div style='flex: 1; padding-right: 10px;'>
            <strong style='color: black;'>{$headerInfo->label}</strong> 
          </div>
          <div style='flex: 1; padding-left: 10px; border-left: 1px solid navy;'>
            <strong style='color: black;'></strong> <span style='border: 1px solid navy; padding: 2px 5px; border-radius: 3px;'>{$headerInfo->headervalue}</span>
          </div>
        </div>
      ";


}


// Close the header container
$header2 .= "</div>";

print $header1. '<br>';
print $header2;


// Split the array into two equal parts
$halfCount = ceil(count($inspectionProducts) / 2);
$firstHalf = array_slice($inspectionProducts, 0, $halfCount);
$secondHalf = array_slice($inspectionProducts, $halfCount);

// Initialize variables for the tables
$table1 = "<div class='table-responsive'>
          <table class='table table-bordered'>
              <thead class='thead-dark'>
                  <tr>
                      <th style='width: 33.33%'>Product Ref</th>
                      <th style='width: 33.33%'>Tag/Categorie</th>
                      <th style='width: 33.33%'>Sub Category</th>
                  </tr>
              </thead>
              <tbody>";
    $table2 = "<div class='table-responsive'>
            <table class='table table-bordered'>
              <thead class='thead-dark'>
                <tr>
                  <th style='width: 33.33%'>Product Ref</th>
                  <th style='width: 33.33%'>Tag/Categorie</th>
                  <th style='width: 33.33%'>Sub Category</th>
                </tr>
              </thead>
              <tbody>";

    // Fill the first table with the first half of the array
    foreach ($firstHalf as $item) {
      $table1 .= "<tr>
      <td style='border-right: 1px solid blue;'><a href='product_view_page.php?product_id={$item->fk_product}'>{$item->label}</a></td>
      <td style='border-right: 1px solid blue;'><a href='product_view_page.php?product_id={$item->{'Tag/Categorie'}}'>{$item->{'categorie'}}</a></td>
              <td>sub-category </td>
            </tr>";
    }
    $table1 .= "</tbody>
          </table>
        </div>";

    // Fill the second table with the second half of the array
    foreach ($secondHalf as $item) {
      $table2 .= "<tr>
      <td style='border-right: 1px solid blue;'><a href='product_view_page.php?product_id={$item->fk_product}'>{$item->label}</a></td>
      <td style='border-right: 1px solid blue;'><a href='product_view_page.php?product_id={$item->{'Tag/Categorie'}}'>{$item->{'categorie'}}</a></td>
              <td>sub-category </td>
            </tr>";
    }
    $table2 .= "</tbody>
          </table>
        </div>";



    // Output both tables
    print "<div class='row'>
          <div class='col-md-6'>$table1</div>
          <div class='col-md-6'>$table2</div>
      </div>";


    // Start table
    print "<table border='1' style='margin: auto;'><tr>";

    // Loop through each item in the array
    foreach ($inspectioncauses as $item) {
      // Print label within table data (td) element with center alignment
      print "<td style='text-align: center;'>{$item->label}</td>";
    }

    // End table row and table
    print "</tr></table>";


          print '</div>';

          print '<div class="tab-pane fade  " id="reducted-view">';

          $header1 = "<br>
          <h2   style='margin-bottom: 10px; color: white; background-color: navy; padding: 10px;'>Header Information</h4>
          <div style='display: flex; flex-wrap: wrap;'>
            <div style='flex: 1; text-align: right; margin-right: 20px;'>
              <h5 style='margin-bottom: 5px;'><strong style='color: navy;'>Date Created  :</strong> {$dateCreated}</h5>
            </div>
            <div style='flex: 1; margin-left: 20px;'>
              <h5 style='margin-bottom: 5px;'><strong style='color: navy;'>Inspection Name  :</strong> {$inspectionname}</h5>
            </div>
            <div style='flex: 1; margin-left: 20px;'>
              <h5 style='margin-bottom: 5px;'><strong style='color: navy;'>Description  :</strong> {$description}</h5>
            </div>
          </div>
        ";
      
      
      
      
      // Start the header information
      $header2 = "<div style='display: flex; flex-wrap: wrap;  margin-left: 10px;'>";
      
      // Loop through each header in the array
      foreach ($inspectionheaders as $headerInfo) {
      // Add the label and value to the header
      
      $header2 .= "
              <div style='margin-bottom: 20px;  margin-left: 20px; padding: 10px; border: 1px solid navy; display: flex; justify-content: center;'>
                <div style='flex: 1; padding-right: 10px;'>
                  <strong style='color: black;'>{$headerInfo->label}</strong> 
                </div>
                <div style='flex: 1; padding-left: 10px; border-left: 1px solid navy;'>
                  <strong style='color: black;'></strong> <span style='border: 1px solid navy; padding: 2px 5px; border-radius: 3px;'>{$headerInfo->headervalue}</span>
                </div>
              </div>
            ";
      
      
      }
      
      
      // Close the header container
      $header2 .= "</div>";
      
      print $header1. '<br>';
      print $header2;
      
      
      // echo '<pre>';
      // var_dump($inspectionProducts);
      // echo '<pre>';
      
      
      // Initialize variables for the tables
              
      // Initialize the table
        $table = "<div class='table-responsive mx-auto' style='right: 200 auto;'>
      
        <table class='table table-bordered'>
            <thead class='thead-dark'>
                <tr>
                    <th>Categorie  Products</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>";

            $categoryCount = 1;
            $productCount = 1;
            
            foreach ($productsByCategory as $category => $products) {
                // Initialize total price for the category
                $categoryTotalPrice = 0;
            
                $table .= "<tr>
                                <td style='font-weight: bold;'> $categoryCount . $category</td>
                                <td></td>
                                <td></td>
                                <td>";
            
                // Calculate total price for the category
                foreach ($products as $product) {
                    // Add price only if status is not "Comply"
                    if ($product->status != "Comply") {
                        $categoryTotalPrice += $product->price_ttc;
                    }
                }
            
                // Display the total price for the category
                $table .= number_format($categoryTotalPrice, 2);
            
                $table .= "</td>
                            </tr>";
            
                foreach ($products as $product) {
                    $table .= "<tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$categoryCount.$productCount <a href='product_view_page.php?product_id={$product->categorieid}'>{$product->label}</a></td>
                                    <td>{$product->status}</td>
                                    <td>{$product->description}</td>
                                    <td>{$product->price_ttc}</td>
                                </tr>";
                    $productCount++;
                }
            
                // Append the total price to the price of the category
                $table = str_replace("$categoryCount . $category</td><td></td><td></td><td>", "$categoryCount . $category</td><td></td><td></td><td>$categoryTotalPrice", $table);
            
                $categoryCount++;
                $productCount = 1;
            }
            
            // Close the table
            $table .= "</tbody>
                        </table>
                    </div>";
            
        // Output the table
        print "<div class='row'>
        <div class='col-md-6'>$table</div>
        </div>";

        // Start table
        print "<table border='1' style='margin: auto;'><tr>";

        // Loop through each item in the array
        foreach ($inspectioncauses as $item) {
        // Print label within table data (td) element with center alignment
        print "<td style='text-align: center;'>{$item->label}</td>";
        }

        // End table row and table
        print "</tr></table>";

        print '</div>';

          print '<div class="tab-pane fade" id="summarised-view">';
          print '<p>Summarised Version content goes here</p>';
          // Your header information and tables for the summarised view go here

          print '</div>';

          print '</div>';
          print '</div>';
