<?php
/* Copyright (C) 2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * or see https://www.gnu.org/
 */

/**
 *	\file			htdocs/core/actions_builddoc.inc.php
 *  \brief			Code for actions on building or deleting documents
 */


// $Objects types must be defined

if (!empty($permissioncreate) && empty($permissiontoadd)) {
	$permissiontoadd = $permissioncreate; // For backward compatibility
}



if($action == 'inspect')
{


    $error= 0;
 



    $fk_inspectiontype = $_POST['fk_inspectiontype'];
    $fk_user_creat = $user->id;

    $inspectiontype = $InspectionType->fetchInspectionType($fk_inspectiontype);


    $InspectionTypevalue = $inspectiontype[0];

    $labelInspectiontype = $InspectionTypevalue->inspectiontype;
    $inspectionrowid = $InspectionTypevalue->rowid;
    $Inspectiontypedescription = $InspectionTypevalue->description;


    // echo '<pre>';
    // var_dump($_POST);
    // echo '<pre>';

    $fk_inspectionsheet  = $_POST['fk_inspectionsheet'];


    foreach ($_POST as $key => $value) {


        $fk_inspectionsheet  = $_POST['fk_inspectionsheet'];
        $fk_propal = $_POST['fk_propal'];

        if (strpos($key, 'headervalue_') === 0) {
            // Extract the number from the key
            $index = substr($key, strlen('headerlabel_'));
    
            // Access the value using the constructed key
            $header_value = $_POST["headervalue_" . $index];
            $fk_inspectionheader = $_POST["fk_inspectionheader_" . $index];
            $fk_inspectiontype = $_POST['fk_inspectiontype'];
            $fk_user_creat = $user->id;
    

            $results = $inspectionheader->insertInpectionHeaders($header_value,$fk_inspectionheader,$fk_inspectiontype,$fk_user_creat,$fk_inspectionsheet);

            if ($results < 0) {
                $error++;
                setEventMessages($langs->trans("ErrorFailedToAddHeaders"), null, 'errors');
            }else
            {

            }
        }
    
        if (strpos($key, 'fk_product_') === 0) {
            // Extract the number from the key
            $index = substr($key, strlen('fk_product_'));
    
            // Access the values using the constructed keys
            $fk_product = $_POST["fk_product_" . $index];
            $label = $_POST["label_" . $index];
            $description = $_POST["description_" . $index];
            $status = $db->escape($_POST['status_' . $index]);
            $fk_user_creat = $user->id;

    
            
            $results = $inspectionTypeLines->insertInpectionLines($fk_product,$label,$fk_inspectiontype,$description,$status,$fk_user_creat,$fk_inspectionsheet);
            
            
            if ($results < 0) {
                $error++;
                setEventMessages($langs->trans("ErrorFailedToAddLineitems"), null, 'errors');
            }else
            {
            }

        }
            


            if (strpos($key, 'fk_product_') === 0) {
                    // Extract the number from the key
                    $index = substr($key, strlen('fk_product_'));
            
                    // Access the values using the constructed keys
                    $fk_product = $db->escape($_POST['fk_product_' . $index]);
                    $label = $db->escape($_POST['label' . $index]);
                    $description = $db->escape($_POST['description_' . $index]);
                    $status = $db->escape($_POST['status_' . $index]);
                    $fk_user_creat = $user->id;
            
            
                    
   
            
                    $objectpropal = new Propal($db);
            
                    if ($fk_propal ) {
                        $ret = $objectpropal->fetch($fk_propal);
                        if ($ret > 0) {
                            $ret = $objectpropal->fetch_thirdparty();
                        }
                        if ($ret <= 0) {
                            setEventMessages($objectpropal->error, $objectpropal->errors, 'errors');
                            $action = '';
                        }
                    }else{
                        // see out error
                    }
            
            
                     
                    if (isset($_POST['repair_amount' . $index]) && $_POST['repair_amount' . $index] !== '') {
                        $repair_amount = $db->escape($_POST['repair_amount' . $index]);
                    } else {
                        $repair_amount = 0;
                    }
                    
                    if (isset($_POST['replace_amount' . $index]) && $_POST['replace_amount' . $index] !== '') {
                        $replace_amount = $db->escape($_POST['replace_amount' . $index]);
                    } else {
                        $replace_amount = 0;
                    }
                    
                    if ($status === 'Replace' || $status === 'Repair') {
                        if ($status === 'Replace') {
                            $inspection_amount = $replace_amount ? $replace_amount : 0;
                        } elseif ($status === 'Repair') {
                            $inspection_amount = $repair_amount ? $repair_amount : 0;
                        }
                    }
                    
            
                    // Set if we used free entry or predefined product
                    $predef = $predef ? $predef : 'predef';
                    $product_desc =  $Inspectiontypedescription;
	
		
                    $prod_entry_mode = "predef";
            
                    if ($prod_entry_mode == 'free') {
                        $idprod = 0;
                    } else {
                        $idprod = $fk_product;
            
                        if (!empty($conf->global->MAIN_DISABLE_FREE_LINES) && $idprod <= 0) {
            
                            setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv("ProductOrService")), null, 'errors');
                            $error++;
                        }
                    }
            
                    $tva_tx = 15; //this is tax, a 0 or 15%
            
                    $qty =  1 ; //  the  equity
                    $remise_percent = 0; // remise is a french word for discount and we will see how our module can implement discont form our customers
                    if (empty($remise_percent)) {
                        $remise_percent = 0;
                    }
            
            
                
            
                    if (!$error && ($qty >= 0) && (!empty($Inspectiontypedescription) || (!empty($idprod) && $idprod > 0))) {
                        
            
                        $price_base_type = 'HT';
            
                        if (!empty($idprod) && $idprod > 0) {
            
                            $prod = new Product($db);
            
                            $prod->fetch($idprod);
            
                            $Inspectiontypedescription = $Inspectiontypedescription;
                            
                                            
                            // $tva_npr = get_default_npr($mysoc, $objectpropal->thirdparty, $prod->id);
                            if (empty($tva_tx)) {
                                $tva_npr = 0;
                            }
            
            
                            if (!empty($repair_amount) && is_numeric($repair_amount)) {
                                $pu_ht += $repair_amount;
                                $pu_ttc += $repair_amount;
                
                            } 
                            if (!empty($replace_amount) && is_numeric($replace_amount)) {
                                $pu_ht2 += $replace_amount;
                                $pu_ttc2 += $replace_amount;
                
                            } 
            
            
            
                            // Price unique per product
            
                            $pu_ht = $prod->price  ;
            
                            $pu_ttc = $prod->price_ttc ;
                            $price_min = $prod->price_min ;
                            $price_min_ttc = $prod->price_min_ttc;
            
                            $pu_ht2 =$pu_ht2 + $prod->price  ;
            
                            $pu_ttc2 = $pu_ttc2 + $prod->price_ttc ;
                            $price_min2 = $price_min2 + $prod->price_min ;
                            $price_min_ttc2 =  $price_min_ttc2 + $prod->price_min_ttc;
                            $price_base_type= $prod->price_base_type;
            
            
            
                            // If price per segment
                            if (!empty($conf->global->PRODUIT_MULTIPRICES) && $objectpropal->thirdparty->price_level) {
            
            
                                $pu_ht = $prod->multiprices[$objectpropal->thirdparty->price_level];
                                $pu_ttc = $prod->multiprices_ttc[$objectpropal->thirdparty->price_level];
                                $price_min = $prod->multiprices_min[$objectpropal->thirdparty->price_level];
                                $price_min_ttc = $prod->multiprices_min_ttc[$objectpropal->thirdparty->price_level];
                                
                                $pu_ht2 += $prod->multiprices[$objectpropal->thirdparty->price_level];
                                $pu_ttc2 += $prod->multiprices_ttc[$objectpropal->thirdparty->price_level];
                                $price_min2 += $prod->multiprices_min[$objectpropal->thirdparty->price_level];
                                $price_min_ttc2 += $prod->multiprices_min_ttc[$objectpropal->thirdparty->price_level];
                                
            
                                $price_base_type = $prod->multiprices_base_type[$objectpropal->thirdparty->price_level];
                                if (!empty($conf->global->PRODUIT_MULTIPRICES_USE_VAT_PER_LEVEL)) {  // using this option is a bug. kept for backward compatibility
                                    if (isset($prod->multiprices_tva_tx[$objectpropal->thirdparty->price_level])) {
                                        $tva_tx = $prod->multiprices_tva_tx[$objectpropal->thirdparty->price_level];
                                    }
                                    if (isset($prod->multiprices_recuperableonly[$objectpropal->thirdparty->price_level])) {
                                        $tva_npr = $prod->multiprices_recuperableonly[$objectpropal->thirdparty->price_level];
                                    }
                                }
                            } elseif (!empty($conf->global->PRODUIT_CUSTOMER_PRICES)) {
                                // If price per customer
                                require_once DOL_DOCUMENT_ROOT.'/product/class/productcustomerprice.class.php';
            
                                $prodcustprice = new Productcustomerprice($db);
                                
            
            
                                $filter = array('t.fk_product' => $prod->id, 't.fk_soc' => $objectpropal->thirdparty->id);
            
                                $result = $prodcustprice->fetchAll('', '', 0, 0, $filter);
                                if ($result) {
                                    // If there is some prices specific to the customer
            
                                    if (count($prodcustprice->lines) > 0) {
                                        $pu_ht = price($prodcustprice->lines[0]->price);
                                        $pu_ttc = price($prodcustprice->lines[0]->price_ttc);
                                        $price_min =  price($prodcustprice->lines[0]->price_min);
                                        $price_min_ttc =  price($prodcustprice->lines[0]->price_min_ttc);
            
                                        $pu_ht2 += price($prodcustprice->lines[0]->price);
                                        $pu_ttc2 += price($prodcustprice->lines[0]->price_ttc);
                                        $price_min2 +=  price($prodcustprice->lines[0]->price_min);
                                        $price_min_ttc2 +=  price($prodcustprice->lines[0]->price_min_ttc);
                                        $price_base_type = $prodcustprice->lines[0]->price_base_type;
                                        $tva_tx = ($prodcustprice->lines[0]->default_vat_code ? $prodcustprice->lines[0]->tva_tx.' ('.$prodcustprice->lines[0]->default_vat_code.' )' : $prodcustprice->lines[0]->tva_tx);
                                        if ($prodcustprice->lines[0]->default_vat_code && !preg_match('/\(.*\)/', $tva_tx)) {
                                            $tva_tx .= ' ('.$prodcustprice->lines[0]->default_vat_code.')';
                                        }
                                        $tva_npr = $prodcustprice->lines[0]->recuperableonly;
                                        if (empty($tva_tx)) {
                                            $tva_npr = 0;
                                        }
                                    }
                                }
                            } elseif (!empty($conf->global->PRODUIT_CUSTOMER_PRICES_BY_QTY)) {
                                // If price per quantity
            
                                if ($prod->prices_by_qty[0]) {	// yes, this product has some prices per quantity
                                    // Search the correct price into loaded array product_price_by_qty using id of array retrieved into POST['pqp'].
            
                                    $pqp = 0;
            
                                    // Search price into product_price_by_qty from $prod->id
                                    foreach ($prod->prices_by_qty_list[0] as $priceforthequantityarray) {
                                        if ($priceforthequantityarray['rowid'] != $pqp) {
                                            continue;
                                        }
                                        // We found the price
                                        if ($priceforthequantityarray['price_base_type'] == 'HT') {
                                            $pu_ht = $priceforthequantityarray['unitprice'];
                                            $pu_ht2 += $priceforthequantityarray['unitprice'];
            
                                        } else {
                                            $pu_ttc = $priceforthequantityarray['unitprice'];
                                            $pu_ttc2 += $priceforthequantityarray['unitprice'];
            
                                        }
                                        // Note: the remise_percent or price by qty is used to set data on form, so we will use value from POST.
                                        break;
                                    }
                                }
                            } elseif (!empty($conf->global->PRODUIT_CUSTOMER_PRICES_BY_QTY_MULTIPRICES)) {
                                // If price per quantity and customer
                                if ($prod->prices_by_qty[$objectpropal->thirdparty->price_level]) { // yes, this product has some prices per quantity
                                    // Search the correct price into loaded array product_price_by_qty using id of array retrieved into POST['pqp'].
                                    $pqp = 0;
            
                                    // Search price into product_price_by_qty from $prod->id
                                    foreach ($prod->prices_by_qty_list[$objectpropal->thirdparty->price_level] as $priceforthequantityarray) {
                                        if ($priceforthequantityarray['rowid'] != $pqp) {
                                            continue;
                                        }
                                        // We found the price
                                        if ($priceforthequantityarray['price_base_type'] == 'HT') {
                                            $pu_ht = $priceforthequantityarray['unitprice'];
                                            $pu_ht2 += $priceforthequantityarray['unitprice'];
            
                                        } else {
                                            $pu_ttc = $priceforthequantityarray['unitprice'];
                                            $pu_ttc2 += $priceforthequantityarray['unitprice'];
            
                                        }
                                        // Note: the remise_percent or price by qty is used to set data on form, so we will use value from POST.
                                        break;
                                    }
                                }
                            }
            
            
                            $tmpvat = price2num(preg_replace('/\s*\(.*\)/', '', $tva_tx));
                            $tmpprodvat = price2num(preg_replace('/\s*\(.*\)/', '', $prod->tva_tx));
            
                            // Set unit price to use
                            if (!empty($prod->price_ht) || (string) $prod->price_ht === '0') {
            
            
                                $pu_ht = price2num($prod->price_ht, 'MU');
                                $pu_ttc = price2num($pu_ht * (1 + ((float) $tmpvat / 100)), 'MU');
            
                                $pu_ht2 += price2num($prod->price_ht, 'MU');
                                $pu_ttc2 += price2num($pu_ht * (1 + ((float) $tmpvat / 100)), 'MU');
                            } elseif (!empty($prod->price_ttc) || (string) $prod->price_ttc === '0') {
            
            
                                $pu_ttc = price2num($prod->price_ttc, 'MU');
                                $pu_ht = price2num($pu_ttc / (1 + ((float) $tmpvat / 100)), 'MU');
            
                                $pu_ttc2 += price2num($prod->price_ttc, 'MU');
                                $pu_ht2 += price2num($pu_ttc / (1 + ((float) $tmpvat / 100)), 'MU');
            
            
            
                            } elseif ($tmpvat != $tmpprodvat) {
                                // Is this still used ?
                                if ($price_base_type != 'HT') {
                                    $pu_ht = price2num($pu_ttc / (1 + ($tmpvat / 100)), 'MU');
                                    $pu_ht2 += price2num($pu_ttc / (1 + ($tmpvat / 100)), 'MU');
            
                                } else {
                                    $pu_ttc = price2num($pu_ht * (1 + ($tmpvat / 100)), 'MU');
                                    $pu_ttc2 += price2num($pu_ht * (1 + ($tmpvat / 100)), 'MU');
            
                                }
                            }
            
                            $desc = $Inspectiontypedescription;
            
            
                            // Define output language
                            if (getDolGlobalInt('MAIN_MULTILANGS') && !empty($conf->global->PRODUIT_TEXTS_IN_THIRDPARTY_LANGUAGE)) {

                                $outputlangs = $langs;
                                $newlang = '';
                             
                                if (empty($newlang)) {
                                    $newlang = $objectpropal->thirdparty->default_lang;
                                }
                                if (!empty($newlang)) {
                                    $outputlangs = new Translate("", $conf);
                                    $outputlangs->setDefaultLang($newlang);
                                }
                                $desc = (!empty($prod->multilangs[$outputlangs->defaultlang]["description"])) ? $prod->multilangs[$outputlangs->defaultlang]["description"] : $inspectiontypeName;
                            } else {
                                $desc = $Inspectiontypedescription;
                            }
            
                            
                            //If text set in desc is the same as product description (as now it's preloaded) whe add it only one time
                            if ($product_desc==$desc && !empty($conf->global->PRODUIT_AUTOFILL_DESC)) {
                                $product_desc='';
                            }
            
                            if (!empty($product_desc) && !empty($conf->global->MAIN_NO_CONCAT_DESCRIPTION)) {
                                $desc = $Inspectiontypedescription;
                            } else {
                                $desc = dol_concatdesc($desc, $product_desc, '', !empty($conf->global->MAIN_CHANGE_ORDER_CONCAT_DESCRIPTION));
                            }
            
                            // Add custom code and origin country into description
                            if (empty($conf->global->MAIN_PRODUCT_DISABLE_CUSTOMCOUNTRYCODE) && (!empty($prod->customcode) || !empty($prod->country_code))) {
                                $tmptxt = '(';
                                // Define output language
            
                                if (getDolGlobalInt('MAIN_MULTILANGS') && !empty($conf->global->PRODUIT_TEXTS_IN_THIRDPARTY_LANGUAGE)) {
                                    $outputlangs = $langs;
                                    $newlang = '';
                                    if (empty($newlang) && GETPOST('lang_id', 'alpha')) {
                                        $newlang = GETPOST('lang_id', 'alpha');
                                    }
                                    if (empty($newlang)) {
                                        $newlang = $objectpropal->thirdparty->default_lang;
                                    }
                                    if (!empty($newlang)) {
                                        $outputlangs = new Translate("", $conf);
                                        $outputlangs->setDefaultLang($newlang);
                                        $outputlangs->load('products');
                                    }
                                    if (!empty($prod->customcode)) {
                                        $tmptxt .= $outputlangs->transnoentitiesnoconv("CustomCode").': '.$prod->customcode;
                                    }
                                    if (!empty($prod->customcode) && !empty($prod->country_code)) {
                                        $tmptxt .= ' - ';
                                    }
                                    if (!empty($prod->country_code)) {
                                        $tmptxt .= $outputlangs->transnoentitiesnoconv("CountryOrigin").': '.getCountry($prod->country_code, 0, $db, $outputlangs, 0);
                                    }
                                } else {
                                    if (!empty($prod->customcode)) {
                                        $tmptxt .= $langs->transnoentitiesnoconv("CustomCode").': '.$prod->customcode;
                                    }
                                    if (!empty($prod->customcode) && !empty($prod->country_code)) {
                                        $tmptxt .= ' - ';
                                    }
                                    if (!empty($prod->country_code)) {
                                        $tmptxt .= $langs->transnoentitiesnoconv("CountryOrigin").': '.getCountry($prod->country_code, 0, $db, $langs, 0);
                                    }
                                }
                                $tmptxt .= ')';
                                $desc = dol_concatdesc($description, $tmptxt);
                            }
            
                            $type = $prod->type;
                            $fk_unit = $prod->fk_unit;
                        } else {
                            $pu_ht = price2num($prod->price_ht, 'MU');
                            $pu_ttc = price2num($prod->price_ttc, 'MU');
            
                            $pu_ht2 += price2num($prod->price_ht, 'MU');
                            $pu_ttc2 += price2num($prod->price_ttc, 'MU');
                            $tva_npr = (preg_match('/\*/', $tva_tx) ? 1 : 0);
                            if (empty($tva_tx)) {
                                $tva_npr = 0;
                            }
                            $tva_tx = str_replace('*', '', $tva_tx);
                            $labelInspectiontype = $labelInspectiontype;
                            $desc = $Inspectiontypedescription;
                            $type =-1;
                            $fk_unit = GETPOST('units', 'alpha') ? GETPOST('units', 'alpha') : 0 ;
                            $pu_ht_devise = price2num($price_ht_devise, 'MU');
                            $pu_ttc_devise = price2num($price_ttc_devise, 'MU');
            
                            $pu_ht_devise2 += price2num($price_ht_devise, 'MU');
                            $pu_ttc_devise2 += price2num($price_ttc_devise, 'MU');
            
            
                            if ($pu_ttc && !$pu_ht) {
                                $price_base_type = 'TTC';
                            }
                        }
            
            
                        // Prepare a price equivalent for minimum price check
                        $pu_equivalent = $pu_ht ;
                        $pu_equivalent_ttc = $pu_ttc;
            
                        $pu_equivalent2 += $pu_ht ;
                        $pu_equivalent_ttc2 += $pu_ttc;
            
            
                        $currency_tx = $objectpropal->multicurrency_tx;
            
                        // Check if we have a foreing currency
            
                        // If so, we update the pu_equiv as the equivalent price in base currency
                        if ($pu_ht == '' && $pu_ht_devise != '' && $currency_tx != '') {
                            $pu_equivalent = $pu_ht_devise * $currency_tx;
                        }
                        if ($pu_ttc == '' && $pu_ttc_devise != '' && $currency_tx != '') {
                            $pu_equivalent_ttc = $pu_ttc_devise * $currency_tx;
                        }
            
                        // Local Taxes
                        $localtax1_tx = get_localtax($tva_tx, 1, $objectpropal->thirdparty, $tva_npr);
                        $localtax2_tx = get_localtax($tva_tx, 2, $objectpropal->thirdparty, $tva_npr);
            
                        $info_bits = 0;
                        if ($tva_npr) {
                            $info_bits |= 0x01;
                        }
                        //var_dump(price2num($price_min)); var_dump(price2num($pu_ht)); var_dump($remise_percent);
                        //var_dump(price2num($price_min_ttc)); var_dump(price2num($pu_ttc)); var_dump($remise_percent);exit;
            
                        if ($usermustrespectpricemin) {
                            if ($pu_equivalent && $price_min && ((price2num($pu_equivalent) * (1 - $remise_percent / 100)) < price2num($price_min))) {
                                $mesg = $langs->trans("CantBeLessThanMinPrice", price(price2num($price_min, 'MU'), 0, $langs, 0, 0, -1, $conf->currency));
                                setEventMessages($mesg, null, 'errors');
                                $error++;
                            } elseif ($pu_equivalent_ttc && $price_min_ttc && ((price2num($pu_equivalent_ttc) * (1 - $remise_percent / 100)) < price2num($price_min_ttc))) {
                                $mesg = $langs->trans("CantBeLessThanMinPrice", price(price2num($price_min_ttc, 'MU'), 0, $langs, 0, 0, -1, $conf->currency));
                                setEventMessages($mesg, null, 'errors');
                                $error++;
                            }
                        }

                        $Inspectiontypedescription = $Inspectiontypedescription;

                    }
                    
                
                    }
            
            
                    // $file_path = 'htdocs/comm/propal/card.php';
                    // $form_action_url = $file_path . '?id=' . urlencode($fk_propal) ;
                    // print '<script>window.location.href = "' . DOL_URL_ROOT . '/comm/propal/card.php?id='.urlencode($fk_propal).'";</script>';
            
                    
            }


        if (!$error) {

            // $objectpropal->table_element_line = 'propaldet';
           


            $result = $object->addInspectionLineItem($Inspectiontypedescription, $pu_ht2, $qty, $tva_tx, $localtax1_tx, $localtax2_tx, $idprod, $remise_percent, $price_base_type, $pu_ttc2, $info_bits, $type, min($rank, count($objectpropal->lines) + 1), 0, GETPOST('fk_parent_line'), $fournprice, $pa_ht, $labelInspectiontype, $date_start, $date_end, $array_options, $fk_unit, '', 0, $pu_ht_devise, $fk_remise_except = 0, $noupdateafterinsertline = 0, $fk_propal,$objectpropal ,$fk_inspectionsheet);
            // $result = $objectpropal->addline($Inspectiontypedescription, $pu_ht2, $qty, $tva_tx, $localtax1_tx, $localtax2_tx, $idprod, $remise_percent, $price_base_type, $pu_ttc2, $info_bits, $type, min($rank, count($objectpropal->lines) + 1), 0, GETPOST('fk_parent_line'), $fournprice, $pa_ht, $labelInspectiontype, $date_start, $date_end, $array_options, $fk_unit, '', 0, $pu_ht_devise, $fk_remise_except = 0, $noupdateafterinsertline = 0,$fk_propal,$objectpropal);


            $objectpropal->id = $result;
            if ($result > 0) {
                $db->commit();

                if (!getDolGlobalString('MAIN_DISABLE_PDF_AUTOUPDATE')) {
                    // Define output language
                    $outputlangs = $langs;
                    if (getDolGlobalInt('MAIN_MULTILANGS')) {
                        $outputlangs = new Translate("", $conf);
                        $newlang = (GETPOST('lang_id', 'aZ09') ? GETPOST('lang_id', 'aZ09') : $objectpropal->thirdparty->default_lang);
                        $outputlangs->setDefaultLang($newlang);
                    }
                    $ret = $objectpropal->fetch($fk_propal); // Reload to get new records
                    if ($ret > 0) {
                        $objectpropal->fetch_thirdparty();
                    }
                    $objectpropal->generateDocument($objectpropal->model_pdf, $outputlangs, $hidedetails, $hidedesc, $hideref);
                }

            } else {
                $db->rollback();

                setEventMessages($object->error, $object->errors, 'errors');
            }

}

    }




    if (isset($_POST['causeid']) && is_array($_POST['cause'])) {
        // Assuming the causes array contains alternating strings and numbers
        for ($i = 0; $i < count($_POST['causeid']); $i++) {
            $causeName = $_POST['cause'][$i];
            $causeId = $_POST['causeid'][$i];
            $fk_user_creat = $user->id;
    
      
            $results = $CausesOfFailure->insertInpectionCauseOfFailure($causeId, $causeName, $fk_inspectiontype, $fk_user_creat,$fk_inspectionsheet);
           
            
            if ($results < 0) {
                $error++;
                setEventMessages($langs->trans("ErrorFailedToAtCauses"), null, 'errors');
            }else
            {

                
            }
        }
    }
    


 


    

