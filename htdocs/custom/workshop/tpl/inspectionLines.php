<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>


<body>
  <!-- Your PHP code and HTML content here -->
</body>
</html>

<?php

// Protection to avoid direct call of template
if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}




print '<div class="container-fluid" id="inspectionlines"  style="Display:none">';


// Desired order of columns
$InspectionColumns = array( 'label', 'status', 'description');

print '<form  id="Inspectionlines" method="POST" action="'.$_SERVER["PHP_SELF"].'">';
print '<input type="hidden" name="token" value="'.newToken().'">';
print '<input type="hidden" name="action" value="add">';
if ($backtopage) {
    print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
}
if ($backtopageforcancel) {
    print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';
}
if ($backtopagejsfields) {
    print '<input type="hidden" name="backtopagejsfields" value="'.$backtopagejsfields.'">';
}
if ($dol_openinpopup) {
    print '<input type="hidden" name="dol_openinpopup" value="'.$dol_openinpopup.'">';
}

// Open a row div with Bootstrap classes
// print '<div class="row">';

// First column for the first table
print '<div class="row">';
print '<div class="col-md-6">';
print '<table class="table table-responsive table-bordered">'."\n";
print '<tr class="field_column">';
$columnCount = 0;

foreach ($InspectionColumns as $column) {
    // Check if we have printed the first three columns
    if ($columnCount >= 3) {
        break;
    }

    // Generate table header cell for each column
    print '<th class="col-4 titlefieldcreate">' . ucwords($column) . '</th>'; // Example: Label, Status, Description, FK Product

    $columnCount++;
}
print '</tr>';




// Continue with the rest of the table rows for the first table

$totalItems = count($uniqueProductsArray);
// echo $totalItems;
$halfCount = ceil($totalItems / 2);



foreach ($uniqueProductsArray as $index => $row) {

    if ($index >= $halfCount) {
        break;
    }

    print '<tr>';
    foreach ($InspectionColumns as $column) {


        
        print '<td class="col-md-4">';

        print '<input type="hidden" name="fk_product_' . $index . '" value="' . $row->fk_product . '">';
        // print '<input type="hidden" name="fk_inspectionheader_' . $index . '" value="' . $row->inspectionheader . '">';


        if ($column === 'category') {
            print '<input type="text" name="category_' . $index . '" value="' . $row->fk_categorie . '" readonly>';
        } elseif ($column === 'label') {
            print '<input type="text" name="label_' . $index . '" value="' . $row->label . '" readonly>';
        } elseif ($column === 'status') {
            $statusName = "status_$index";        
            print '<select name="' . $statusName . '">';
            print '<option value="Comply"' . ($row->column === 'Comply' ? ' selected' : '') . '>Comply</option>';
            print '<option value="Repair"' . ($row->column === 'Repair' ? ' selected' : '') . '>Repair</option>';
            print '<option value="Replace"' . ($row->column === 'Replace' ? ' selected' : '') . '>Replace</option>';
            print '</select>';
        } elseif ($column === 'description') {

            $inputName = "description_$index"; 
            print '<input type="text" name="' . $inputName . '" value="" >'; 
        }

        print '</td>';
    }
    print '</tr>';
}

print '</table>';
print '</div>';

// Second column for the second table
print '<div class="col-md-6">';
print '<table class="table table-responsive table-bordered">'."\n";
print '<tr class="field_column">';
$columnCount = 0;
$columnCount = 0;
foreach ($InspectionColumns as $column) {
    // Check if we have printed the first three columns
    if ($columnCount >= 3) {
        break;
    }

    // Generate table header cell for each column
    print '<th class="col-md-4 titlefieldcreate" style="width: 33.33%" >' . ucwords($column) . '</th>'; // Example: Label, Status, Description, FK Product

    $columnCount++;
}

// Fill in empty cells if necessary to ensure equal columns
while ($columnCount < 3) {
    print '<th class="col-4 titlefieldcreate" >-</th>'; // Placeholder column
    $columnCount++;
}

print '</tr>';


// Continue with the rest of the table rows for the second table



foreach ($uniqueProductsArray as $index => $row) {
    if ($index < $halfCount ) {
        continue;
    }


    print '<tr>';
    foreach ($InspectionColumns as $column) {


        print '<td class="col-md-4">';


        print '<input type="hidden" name="fk_product_' . $index . '" value="' . $row->fk_product . '">';
        print '<input type="hidden" name="fk_inspectionheader_' . $index . '" value="' . $row->inspectionheader . '">';



        if ($column === 'product_id') {
            print '<input type="text" name="product_id_' . $index . '" value="' . $row->column . '" readonly>';
        } elseif ($column === 'label') {
            print '<input type="text" name="label_' . $index . '" value="' . $row->label . '" readonly>';
        } elseif ($column === 'status') {
            $statusName = "status_$index";        
            print '<select name="' . $statusName . '">';
            print '<option value="Comply"' . ($row->column === 'Comply' ? ' selected' : '') . '>Comply</option>';
            print '<option value="Repair"' . ($row->column === 'Repair' ? ' selected' : '') . '>Repair</option>';
            print '<option value="Replace"' . ($row->column === 'Replace' ? ' selected' : '') . '>Replace</option>';
            print '</select>';
        } elseif ($column === 'description') {
            $inputName = "description_$index"; 
            print '<input type="text" name="' . $inputName . '" value="" >'; 
        }

        print '</td>';
    }
    print '</tr>';
}

print '</table>';
print '</div>';
    print '</tr>';


print '</table>';
print '</div>';

// Close the row div
print '</div>';

print '<hr>';


print '<div class="checkboxes-container" id ="inspectioncauses" style= Display:none>'; // Start a container for the checkboxes



foreach ($inspectioncauses as $cause) {

    print '<label>';
    print '<input type="checkbox"   name="causeid[]" value="' . $cause->rowid . '" > ' . $cause->label;
    print '<input type="hidden"   name="cause[]" value="' . $cause->label . '" > ';

    print '</label>';
}
print '</div>';




