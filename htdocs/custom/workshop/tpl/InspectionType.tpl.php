<?php
/* Copyright (C) 2017  Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * Need to have following variables defined:
 * $object (invoice, order, ...)
 * $action
 * $conf
 * $langs
 * $form
 */

// Protection to avoid direct call of template
if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}


$object->fields = dol_sort_array($object->fields, 'position');

// echo '<pre>';
// var_dump($object->fields);
// echo '<pre>';
// Integration of the provided code into the loop for displaying form fields
// Loop through other form fields
foreach ($object->fields as $key => $val) {
    // Discard if field is a hidden field on form
    if (abs($val['visible']) != 1 && abs($val['visible']) != 3) {
        continue;
    }

    if (array_key_exists('enabled', $val) && isset($val['enabled']) && !verifCond($val['enabled'])) {
        continue; // We don't want this field
    }

    $value = '';

    // Skip the category field
    if ($key == 'categories') {


        if (isModEnabled('categorie')) {

        print '<tr><td>' . $langs->trans("Categories") . '</td><td>';
        $cate_arbo = $form->select_all_categories(Categorie::TYPE_PRODUCT, '', 'parent', 64, 0, 1);
        print img_picto('', 'category', 'class="pictofixedwidth"') . $form->multiselectarrayinspection('categories', $cate_arbo, GETPOST('categories', 'array'), '', 0, 'quatrevingtpercent widthcentpercentminusx', 0, 0);
        print "</td></tr>";
        }

        continue;
    }

    // Display other form fields
    print '<tr class="field_'.$key.'">';
    print '<td';
    print ' class="titlefieldcreate';
    if (isset($val['notnull']) && $val['notnull'] > 0) {
        print ' fieldrequired';
    }
    if ($val['type'] == 'text' || $val['type'] == 'html') {
        print ' tdtop';
    }
    print '"';
    print '>';
    if (!empty($val['help'])) {
        print $form->textwithpicto($langs->trans($val['label']), $langs->trans($val['help']));
    } else {
        print $langs->trans($val['label']);
    }
    print '</td>';
    print '<td class="valuefieldcreate">';
    if (!empty($val['picto'])) {
        print img_picto('', $val['picto'], '', false, 0, 0, '', 'pictofixedwidth');
    }
    // Retrieve and process the field value based on its type
    // (Code for processing field values goes here)
    // Show input fields based on field type
    if (!empty($val['noteditable'])) {
        print $object->showOutputField($val, $key, $value, '', '', '', 0);
    } else {
        if ($key == 'lang') {
            print img_picto('', 'language', 'class="pictofixedwidth"');
            print $formadmin->select_language($value, $key, 0, null, 1, 0, 0, 'minwidth300', 2);
        } else {
            print $object->showInputField($val, $key, $value, '', '', '', 0);
        }
    }
    print '</td>';
    print '</tr>';
}




?>
<!-- END PHP TEMPLATE commonfields_add.tpl.php -->





