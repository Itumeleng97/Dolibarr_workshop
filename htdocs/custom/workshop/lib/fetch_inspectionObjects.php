<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>


<body>
  <!-- Your PHP code and HTML content here -->
</body>
</html>


<?php
/* Copyright (C) 2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * or see https://www.gnu.org/
 */

/**
 *	\file			htdocs/core/actions_builddoc.inc.php
 *  \brief			Code for actions on building or deleting documents
 */


// $Objects types must be defined

if (!empty($permissioncreate) && empty($permissiontoadd)) {
	$permissiontoadd = $permissioncreate; // For backward compatibility
}



$inspectionsheet  = $object->fetchinspectionsheets();


$inspectiontypes = $InspectionType->fetchInspectionTypes();




if (isset($inspectiontypes) && is_array($inspectiontypes)) {
    $categoryIDs = array(); // Initialize an empty array to store category IDs

    foreach ($inspectiontypes as $object) {
        if (isset($object->categories)) {
            $rowData = array(
                'categoryID' => $object->categories,
                'inspectionypeName' => $object->inspectiontype,
                'rowid' => $object->rowid
            );
    
            // Add the row data array to the organized array
            $organizedArray[] = $rowData;
        }
                 

    }

    print '<div class="table-responsive">';
    print '<table class="table table-bordered table-striped">';
    print '<thead class="thead-dark">';
    print '<tr>';
    
    foreach ($inspectionsheet[0] as $property => $value) {
        print '<th>' . ucwords(str_replace('_', ' ', $property)) . '</th>';
    }
    
    print '</tr>';
    print '</thead>';
    print '<tbody>';
    
    
    foreach ($inspectionsheet as $object) {
        print '<tr>';
    
        foreach ($object as $property => $value) {
            if ($property === 'Ref') {
                print '<td><a href="'.DOL_URL_ROOT.'/custom/workshop/inspectionsheet_card.php?id= '.$value.'&action=view" ><strong>'.$langs->transnoentitiesnoconv("Inspection-").$value.'</strong>/a></td>';
            } else {
                print '<td>' . $value . '</td>';
            }
        }
    
        print '</tr>';
    }
    
    print '</tbody>';
    print '</table>';
    print '</div>';
    
    

  print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'?id='.$objectpropal->id.'&action=createinsp">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="action" value="createinsp">';
	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	}
	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';
	}
	if ($backtopagejsfields) {
		print '<input type="hidden" name="backtopagejsfields" value="'.$backtopagejsfields.'">';
	}
	if ($dol_openinpopup) {
		print '<input type="hidden" name="dol_openinpopup" value="'.$dol_openinpopup.'">';
	}
    print '<input type="hidden" name="fk_propal" value="'. $objectpropal->id.'">';

   


	print dol_get_fiche_head(array(),'');

    print '<div>';
    print '<label for="name">InspectionSheetName:</label>';
    print '<input type="text" id="name" name="name">';
    print '</div>';

    print '<div>';
    print '<label for="description">Description:</label>';
    print '<textarea id="description" name="description" style="width: 100%; height: 100px; resize: vertical;"></textarea>';
    print '</div>';



    print '<label for="inspectionTypeDropdown">Select New Inspection Type:</label>';
    print '<select id="inspectionTypeDropdown" name= "inspectiontype">';
    print '<option value="" selected disabled>Select Inspection Type</option>';
    print ' </select>';
  
    print ' <input type="hidden" id="categoryID" name = "categoryID">';
    print '<input type="hidden" id="rowid" name="rowid">';
  

	print '</table>'."\n";

	print dol_get_fiche_end();

	print $form->buttonsSaveCancel("Create");

	print '</form>';

}









?>
<script>
  $(document).ready(function() {
    var organizedArray = <?php echo json_encode($organizedArray); ?>;

    // Populate dropdown options
    $.each(organizedArray, function(index, item) {
      $('#inspectionTypeDropdown').append($('<option>', { 
        value: index,
        text: item.inspectionypeName // Set the inspection type name as the option text
      }));
    });

    // Handle change event of dropdown
    $('#inspectionTypeDropdown').change(function() {
      var selectedIndex = $(this).val();
      if (selectedIndex !== null) {
        // Get selected inspection type object
        var selectedType = organizedArray[selectedIndex];

        // Set hidden field values
        $('#categoryID').val(selectedType.categoryID);
        $('#rowid').val(selectedType.rowid);
      }
    });
  });
</script>
