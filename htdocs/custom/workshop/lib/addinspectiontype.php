<?php
/* Copyright (C) 2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * or see https://www.gnu.org/
 */

/**
 *	\file			htdocs/core/actions_builddoc.inc.php
 *  \brief			Code for actions on building or deleting documents
 */


// $Objects types must be defined




if (!empty($permissioncreate) && empty($permissiontoadd)) {
	$permissiontoadd = $permissioncreate; // For backward compatibility
}


  

    if(isset($_POST['inspectiontype']) && isset($_POST['categoryID']))
    {


            $object->inspectiontypeName = $_POST['inspectiontype'];
            $object->fk_inspectiontype = $_POST['rowid'];
            $object->fk_categorie = $_POST['categoryID'];
            $object->fk_propal = $_POST['fk_propal'];
            $object->inspectionsheetname = $_POST['name'];
            $object->description = $_POST['description'];
            $object->fk_user_creat = $user->id;


            $results = $object->createInspectionsheet();



            if ($results < 0) {
                $error++;
                setEventMessages($langs->trans("ErrorFailedToAddInspectionsheettype"), null, 'errors');
            }else
            {

                $last_insertedid = $results;
            }





    }



$inspectiontypes = $InspectionType->fetchInspectionType($fk_inspectiontype);


$objectinspection = $inspectiontypes[0];

// Accessing the properties of the object
$fk_inspectiontype = $objectinspection->rowid;
$Inspectionsheettype = $objectinspection->inspectiontype;



$inspectionheader = $inspectionheader->fetchInspectionHeaders($fk_categorie);








// Grouping the data by 'fk_inspectionheader' field
$headerIds = [];

foreach ($inspectionheader as $item) {
    $headerId = $item->fk_inspectionheader;


    // If the header ID is not already in the grouped array, initialize it
    if (!isset($headerIds[$headerId]) ) {


        $headerIds[] = $headerId;
    
		}
}



$inspectionItems = $object->fetchInspectionProductByTags($headerIds);

// echo '<pre>';
// var_dump($inspectionItems);
// echo '<pre>';


// Create a new associative array to store unique fk_product values
$uniqueProducts = [];

// Iterate through the input array
foreach ($inspectionItems as $object) {
    // Use fk_product as the key in the associative array
    // Overwrite previous entries with the same key
    $uniqueProducts[$object->fk_product] = $object;
}

// Convert the associative array back to indexed array
$uniqueProductsArray = array_values($uniqueProducts);

// echo '<pre>';
// var_dump($uniqueProductsArray);
// echo '<pre>';

$inspectioncauses = $CausesOfFailure->fetchInspectionCauses($fk_categorie);

