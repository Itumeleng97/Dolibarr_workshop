<?php
/* Copyright (C) 2017  Laurent Destailleur      <eldy@users.sourceforge.net>
 * Copyright (C) 2023  Frédéric France          <frederic.france@netlogic.fr>
 * Copyright (C) 2024 SuperAdmin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file        class/inspectionsheet.class.php
 * \ingroup     workshop
 * \brief       This file is a CRUD class file for Inspectionsheet (Create/Read/Update/Delete)
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT.'/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';

/**
 * Class for Inspectionsheet
 */
class Inspectionsheet extends CommonObject
{
	/**
	 * @var string ID of module.
	 */
	public $module = 'workshop';

	/**
	 * @var string ID to identify managed object.
	 */
	public $element = 'inspectionsheet';

	/**
	 * @var string Name of table without prefix where object is stored. This is also the key used for extrafields management.
	 */
	public $table_element = 'workshop_inspectionsheet';

	/**
	 * @var int  	Does this object support multicompany module ?
	 * 0=No test on entity, 1=Test with field entity, 'field@table'=Test with link by field@table
	 */
	public $ismultientitymanaged = 0;

	/**
	 * @var int  Does object support extrafields ? 0=No, 1=Yes
	 */
	public $isextrafieldmanaged = 1;

	/**
	 * @var string String with name of icon for inspectionsheet. Must be a 'fa-xxx' fontawesome code (or 'fa-xxx_fa_color_size') or 'inspectionsheet@workshop' if picto is file 'img/object_inspectionsheet.png'.
	 */
	public $picto = 'fa-file';


	const STATUS_DRAFT = 0;
	const STATUS_VALIDATED = 1;
	const STATUS_CANCELED = 9;

	/**
	 *  'type' field format:
	 *  	'integer', 'integer:ObjectClass:PathToClass[:AddCreateButtonOrNot[:Filter[:Sortfield]]]',
	 *  	'select' (list of values are in 'options'),
	 *  	'sellist:TableName:LabelFieldName[:KeyFieldName[:KeyFieldParent[:Filter[:CategoryIdType[:CategoryIdList[:SortField]]]]]]',
	 *  	'chkbxlst:...',
	 *  	'varchar(x)',
	 *  	'text', 'text:none', 'html',
	 *   	'double(24,8)', 'real', 'price', 'stock',
	 *  	'date', 'datetime', 'timestamp', 'duration',
	 *  	'boolean', 'checkbox', 'radio', 'array',
	 *  	'mail', 'phone', 'url', 'password', 'ip'
	 *		Note: Filter must be a Dolibarr Universal Filter syntax string. Example: "(t.ref:like:'SO-%') or (t.date_creation:<:'20160101') or (t.status:!=:0) or (t.nature:is:NULL)"
	 *  'label' the translation key.
	 *  'picto' is code of a picto to show before value in forms
	 *  'enabled' is a condition when the field must be managed (Example: 1 or 'getDolGlobalInt("MY_SETUP_PARAM")' or 'isModEnabled("multicurrency")' ...)
	 *  'position' is the sort order of field.
	 *  'notnull' is set to 1 if not null in database. Set to -1 if we must set data to null if empty ('' or 0).
	 *  'visible' says if field is visible in list (Examples: 0=Not visible, 1=Visible on list and create/update/view forms, 2=Visible on list only, 3=Visible on create/update/view form only (not list), 4=Visible on list and update/view form only (not create). 5=Visible on list and view only (not create/not update). Using a negative value means field is not shown by default on list but can be selected for viewing)
	 *  'noteditable' says if field is not editable (1 or 0)
	 *  'alwayseditable' says if field can be modified also when status is not draft ('1' or '0')
	 *  'default' is a default value for creation (can still be overwrote by the Setup of Default Values if field is editable in creation form). Note: If default is set to '(PROV)' and field is 'ref', the default value will be set to '(PROVid)' where id is rowid when a new record is created.
	 *  'index' if we want an index in database.
	 *  'foreignkey'=>'tablename.field' if the field is a foreign key (it is recommended to name the field fk_...).
	 *  'searchall' is 1 if we want to search in this field when making a search from the quick search button.
	 *  'isameasure' must be set to 1 or 2 if field can be used for measure. Field type must be summable like integer or double(24,8). Use 1 in most cases, or 2 if you don't want to see the column total into list (for example for percentage)
	 *  'css' and 'cssview' and 'csslist' is the CSS style to use on field. 'css' is used in creation and update. 'cssview' is used in view mode. 'csslist' is used for columns in lists. For example: 'css'=>'minwidth300 maxwidth500 widthcentpercentminusx', 'cssview'=>'wordbreak', 'csslist'=>'tdoverflowmax200'
	 *  'help' and 'helplist' is a 'TranslationString' to use to show a tooltip on field. You can also use 'TranslationString:keyfortooltiponlick' for a tooltip on click.
	 *  'showoncombobox' if value of the field must be visible into the label of the combobox that list record
	 *  'disabled' is 1 if we want to have the field locked by a 'disabled' attribute. In most cases, this is never set into the definition of $fields into class, but is set dynamically by some part of code.
	 *  'arrayofkeyval' to set a list of values if type is a list of predefined values. For example: array("0"=>"Draft","1"=>"Active","-1"=>"Cancel"). Note that type can be 'integer' or 'varchar'
	 *  'autofocusoncreate' to have field having the focus on a create form. Only 1 field should have this property set to 1.
	 *  'comment' is not used. You can store here any text of your choice. It is not used by application.
	 *	'validate' is 1 if you need to validate the field with $this->validateField(). Need MAIN_ACTIVATE_VALIDATION_RESULT.
	 *  'copytoclipboard' is 1 or 2 to allow to add a picto to copy value into clipboard (1=picto after label, 2=picto after value)
	 *
	 *  Note: To have value dynamic, you can set value to 0 in definition and edit the value on the fly into the constructor.
	 */

	// BEGIN MODULEBUILDER PROPERTIES
	/**
	 * @var array  Array with all fields and their property. Do not use it as a static var. It may be modified by constructor.
	 */
	public $fields=array(
		"rowid" => array("type"=>"integer", "label"=>"TechnicalID", "enabled"=>"1", 'position'=>1, 'notnull'=>1, "visible"=>"0", "noteditable"=>"1", "index"=>"1", "css"=>"left", "comment"=>"Id"),
		"date_creation" => array("type"=>"datetime", "label"=>"DateCreation", "enabled"=>"1", 'position'=>500, 'notnull'=>1, "visible"=>"-2",),
		"tms" => array("type"=>"timestamp", "label"=>"DateModification", "enabled"=>"1", 'position'=>501, 'notnull'=>0, "visible"=>"-2",),
		"fk_user_creat" => array("type"=>"integer:User:user/class/user.class.php", "label"=>"UserAuthor", "picto"=>"user", "enabled"=>"1", 'position'=>510, 'notnull'=>1, "visible"=>"-2", "csslist"=>"tdoverflowmax150",),
		"fk_user_modif" => array("type"=>"integer:User:user/class/user.class.php", "label"=>"UserModif", "picto"=>"user", "enabled"=>"1", 'position'=>511, 'notnull'=>-1, "visible"=>"-2", "csslist"=>"tdoverflowmax150",),
		"last_main_doc" => array("type"=>"varchar(255)", "label"=>"LastMainDoc", "enabled"=>"1", 'position'=>600, 'notnull'=>0, "visible"=>"0",),
		"import_key" => array("type"=>"varchar(14)", "label"=>"ImportId", "enabled"=>"1", 'position'=>1000, 'notnull'=>-1, "visible"=>"-2",),
		"model_pdf" => array("type"=>"varchar(255)", "label"=>"Model pdf", "enabled"=>"1", 'position'=>1010, 'notnull'=>-1, "visible"=>"0",),
		"fk_propal" => array("type"=>"integer:propal:comm/propal/class/propal.class.php", "label"=>"fk_propal", "enabled"=>"1", 'position'=>50, 'notnull'=>0, "visible"=>"0",),
		"fk_inspectiontype" => array("type"=>"integer:inspectiontype:htdocs/custom/workshop/class/inspectiontype.class.php", "label"=>"fk_inspectiontype", "enabled"=>"1", 'position'=>44, 'notnull'=>1, "visible"=>"-1",),
		"inspectiontypename" => array("type"=>"varchar(128)", "label"=>"inspectiontypeName", "enabled"=>"1", 'position'=>88, 'notnull'=>0, "visible"=>"1",),
		"inspectionsheetname" => array("type"=>"varchar(128)", "label"=>"InspectionSheetName", "enabled"=>"1", 'position'=>81, 'notnull'=>1, "visible"=>"1",),
		"description" => array("type"=>"varchar(128)", "label"=>"Description", "enabled"=>"1", 'position'=>79, 'notnull'=>1, "visible"=>"1",),
	);
	public $rowid;
	public $date_creation;
	public $tms;
	public $fk_user_creat;
	public $fk_user_modif;
	public $last_main_doc;
	public $import_key;
	public $model_pdf;
	public $fk_propal;
	public $fk_inspectiontype;
	public $inspectiontypename;
	public $inspectionsheetname;
	public $description;
	// END MODULEBUILDER PROPERTIES


	// If this object has a subtable with lines

	// /**
	//  * @var string    Name of subtable line
	//  */
	// public $table_element_line = 'workshop_inspectionsheetline';

	// /**
	//  * @var string    Field with ID of parent key if this object has a parent
	//  */
	// public $fk_element = 'fk_inspectionsheet';

	// /**
	//  * @var string    Name of subtable class that manage subtable lines
	//  */
	// public $class_element_line = 'Inspectionsheetline';

	// /**
	//  * @var array	List of child tables. To test if we can delete object.
	//  */
	// protected $childtables = array('mychildtable' => array('name'=>'Inspectionsheet', 'fk_element'=>'fk_inspectionsheet'));

	// /**
	//  * @var array    List of child tables. To know object to delete on cascade.
	//  *               If name matches '@ClassNAme:FilePathClass;ParentFkFieldName' it will
	//  *               call method deleteByParentField(parentId, ParentFkFieldName) to fetch and delete child object
	//  */
	// protected $childtablesoncascade = array('workshop_inspectionsheetdet');

	// /**
	//  * @var InspectionsheetLine[]     Array of subtable lines
	//  */
	// public $lines = array();



	/**
	 * Constructor
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		global $langs;

		$this->db = $db;

		if (!getDolGlobalInt('MAIN_SHOW_TECHNICAL_ID') && isset($this->fields['rowid']) && !empty($this->fields['ref'])) {
			$this->fields['rowid']['visible'] = 0;
		}
		if (!isModEnabled('multicompany') && isset($this->fields['entity'])) {
			$this->fields['entity']['enabled'] = 0;
		}

		// Example to show how to set values of fields definition dynamically
		/*if ($user->hasRight('workshop', 'inspectionsheet', 'read')) {
			$this->fields['myfield']['visible'] = 1;
			$this->fields['myfield']['noteditable'] = 0;
		}*/

		// Unset fields that are disabled
		foreach ($this->fields as $key => $val) {
			if (isset($val['enabled']) && empty($val['enabled'])) {
				unset($this->fields[$key]);
			}
		}

		// Translate some data of arrayofkeyval
		if (is_object($langs)) {
			foreach ($this->fields as $key => $val) {
				if (!empty($val['arrayofkeyval']) && is_array($val['arrayofkeyval'])) {
					foreach ($val['arrayofkeyval'] as $key2 => $val2) {
						$this->fields[$key]['arrayofkeyval'][$key2] = $langs->trans($val2);
					}
				}
			}
		}
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  int 	$notrigger 0=launch triggers after, 1=disable triggers
	 * @return int             Return integer <0 if KO, Id of created object if OK
	 */
	public function create(User $user, $notrigger = 0)
	{
		$resultcreate = $this->createCommon($user, $notrigger);

		//$resultvalidate = $this->validate($user, $notrigger);

		return $resultcreate;
	}

	/**
	 * Clone an object into another one
	 *
	 * @param  	User 	$user      	User that creates
	 * @param  	int 	$fromid     Id of object to clone
	 * @return 	mixed 				New object created, <0 if KO
	 */
	public function createFromClone(User $user, $fromid)
	{
		global $langs, $extrafields;
		$error = 0;

		dol_syslog(__METHOD__, LOG_DEBUG);

		$object = new self($this->db);

		$this->db->begin();

		// Load source object
		$result = $object->fetchCommon($fromid);
		if ($result > 0 && !empty($object->table_element_line)) {
			$object->fetchLines();
		}

		// get lines so they will be clone
		//foreach($this->lines as $line)
		//	$line->fetch_optionals();

		// Reset some properties
		unset($object->id);
		unset($object->fk_user_creat);
		unset($object->import_key);

		// Clear fields
		if (property_exists($object, 'ref')) {
			$object->ref = empty($this->fields['ref']['default']) ? "Copy_Of_".$object->ref : $this->fields['ref']['default'];
		}
		if (property_exists($object, 'label')) {
			$object->label = empty($this->fields['label']['default']) ? $langs->trans("CopyOf")." ".$object->label : $this->fields['label']['default'];
		}
		if (property_exists($object, 'status')) {
			$object->status = self::STATUS_DRAFT;
		}
		if (property_exists($object, 'date_creation')) {
			$object->date_creation = dol_now();
		}
		if (property_exists($object, 'date_modification')) {
			$object->date_modification = null;
		}
		// ...
		// Clear extrafields that are unique
		if (is_array($object->array_options) && count($object->array_options) > 0) {
			$extrafields->fetch_name_optionals_label($this->table_element);
			foreach ($object->array_options as $key => $option) {
				$shortkey = preg_replace('/options_/', '', $key);
				if (!empty($extrafields->attributes[$this->table_element]['unique'][$shortkey])) {
					//var_dump($key);
					//var_dump($clonedObj->array_options[$key]); exit;
					unset($object->array_options[$key]);
				}
			}
		}

		// Create clone
		$object->context['createfromclone'] = 'createfromclone';
		$result = $object->createCommon($user);
		if ($result < 0) {
			$error++;
			$this->setErrorsFromObject($object);
		}

		if (!$error) {
			// copy internal contacts
			if ($this->copy_linked_contact($object, 'internal') < 0) {
				$error++;
			}
		}

		if (!$error) {
			// copy external contacts if same company
			if (!empty($object->socid) && property_exists($this, 'fk_soc') && $this->fk_soc == $object->socid) {
				if ($this->copy_linked_contact($object, 'external') < 0) {
					$error++;
				}
			}
		}

		unset($object->context['createfromclone']);

		// End
		if (!$error) {
			$this->db->commit();
			return $object;
		} else {
			$this->db->rollback();
			return -1;
		}
	}


	




	public function createInspectionsheet()

{	
	global $conf, $langs, $hookmanager;
		$langs->load('workshop');



		$error = 0;

		dol_syslog(get_class($this).'::createInspectionsheet', LOG_DEBUG);

		// Clean parameters
		$this->inspectiontypeName = trim($this->inspectiontypeName);
		$this->fk_categorie = trim($this->fk_categorie);
		$this->fk_user_creat = (int)($this->fk_user_creat);
		$this->fk_propal = (int) $this->fk_propal;
		$this->fk_inspectiontype = (int)($this->fk_inspectiontype);

		$this->db->begin();
		
		$now = dol_now();
		
		$sql = "INSERT INTO " . MAIN_DB_PREFIX . "workshop_inspectionsheet (";
		$sql .= "fk_inspectiontype,";
		$sql .= " fk_propal,";
		$sql .= " inspectionname,";
		$sql .= " description,";
		$sql .= " fk_categorie,";
		$sql .= " date_creation,";
		$sql .= " fk_user_creat";
		$sql .= ") VALUES (";
		$sql .= (int) $this->fk_inspectiontype . ",";
		$sql .= (int) $this->fk_propal . ", "; // Removed extra comma
		$sql .= "'" . $this->db->escape($this->inspectionsheetname) . "',"; // Added missing comma
		$sql .= "'" . $this->db->escape($this->description) . "',"; // Added missing comma
		$sql .= (int) $this->fk_categorie . ", ";
		$sql .= "'" . $this->db->idate($now) . "', "; // Added missing comma
		$sql .= (int) $this->fk_user_creat;
		$sql .= ")";

		$res = $this->db->query($sql);
		if ($res) {
			$id = $this->db->last_insert_id(MAIN_DB_PREFIX."workshop_inspectionsheet");

			if ($id > 0) {
				$this->id = $id;

				if (!$error) {
					$this->db->commit();
					return $id;
				} 
			} 
		} else {

			echo 'We are still here';
			$this->error = $this->db->error();

			$this->error = "Error ".$this->db->lasterror();
			$this->errors[] = $this->error;

			dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
			
			var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);


			$this->db->rollback();
			return -1;
		}





}




			/**
	 * Load object lines in memory from the database
	 *
	 * @param	int		$noextrafields	0=Default to load extrafields, 1=No extrafields
	 * @return 	int         			Return integer <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetchInspectionlinesdata($fk_inspectionsheet)
	{
		
		global $conf, $langs;
		$error = 0;





		$sql = 'SELECT 
						hd.fk_inspectionheader,
						hd.header_value,
						hd.fk_inspectiontype,
						hd.fk_inspectionsheet,
						wi.rowid,
						wi.label as inspectionheader,
						wi.type

				FROM 
							'.MAIN_DB_PREFIX.'workshop_inspectionheader  as wi

						LEFT JOIN 
							'.MAIN_DB_PREFIX.'inspectionheaders as hd ON  wi.rowid = hd.fk_inspectionheader


						WHERE hd.fk_inspectionsheet = '.$fk_inspectionsheet;

		
		dol_syslog(__METHOD__, LOG_DEBUG);
		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			if ($num) {
				while ($obj = $this->db->fetch_object($resql)) {
					$this->lines_display[] = $obj;
				}
			}


			return $this->lines_display;
		} else {

			$this->error = "Error ".$this->db->lasterror();
			$this->errors[] = $this->error;

			dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
			
			var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);


			return -1;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param 	int    	$id   			Id object
	 * @param 	string 	$ref  			Ref
	 * @param	int		$noextrafields	0=Default to load extrafields, 1=No extrafields
	 * @param	int		$nolines		0=Default to load extrafields, 1=No extrafields
	 * @return 	int     				Return integer <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetch($id, $ref = null, $noextrafields = 0, $nolines = 0)
	{
		$result = $this->fetchCommon($id, $ref, '', $noextrafields);
		if ($result > 0 && !empty($this->table_element_line) && empty($nolines)) {
			$this->fetchLines($noextrafields);
		}
		return $result;
	}

	/**
	 * Load object lines in memory from the database
	 *
	 * @param	int		$noextrafields	0=Default to load extrafields, 1=No extrafields
	 * @return 	int         			Return integer <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetchLines($noextrafields = 0)
	{
		$this->lines = array();

		$result = $this->fetchLinesCommon('', $noextrafields);
		return $result;
	}


	/**
	 * load  inspection  from table worksho_inspectionsheet 
	 * @return 	int     				Return integer <0 if KO, 0 if not found, >0 if OK
	 * 
	 */

	 public function  fetchinspectionsheets()
	 {

		global $user;

		$lines_display = array();

		$sql = "SELECT rowid as Ref, fk_inspectiontype as InspectionType, fk_categorie as `Tag/Categorie`, fk_propal as Proposal, date_creation as Date_Created
				FROM " . MAIN_DB_PREFIX . "workshop_inspectionsheet";
		
				dol_syslog(__METHOD__, LOG_DEBUG);
				$resql = $this->db->query($sql);
				if ($resql) {
					$num = $this->db->num_rows($resql);
					if ($num) {
						while ($obj = $this->db->fetch_object($resql)) {
						$lines_display[] = $obj;
						}
					}


					return $lines_display;
				} else {

					$this->error = "Error ".$this->db->lasterror();
					$this->errors[] = $this->error;

					dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
					


					return -1;
				}
	 }




	/**
	 * fetch products from table products based on their categories
	 * @param 	int    	$id   			Id object
	 * @param 	string 	$ref  			Ref
	 * @return 	int     				Return integer <0 if KO, 0 if not found, >0 if OK
	 * 
	 */

	 public function  fetchProductByTags($fk_categorie)
	 {

		global $user;

		$lines_display = array();

		$sql = 'SELECT  
						cp.fk_categorie,
						cp.fk_product , 
						ps.rowid,
						ps.label ,
					
			 FROM  
				'.MAIN_DB_PREFIX.'categorie_product as cp 

					LEFT JOIN 

				'.MAIN_DB_PREFIX.'product as ps ON cp.fk_product = ps.rowid  
					
				
				WHERE cp.fk_categorie = '.$fk_categorie.'
				';



				// echo $sql;
		
				dol_syslog(__METHOD__, LOG_DEBUG);
				$resql = $this->db->query($sql);
				if ($resql) {
					$num = $this->db->num_rows($resql);
					if ($num) {
						while ($obj = $this->db->fetch_object($resql)) {
						$lines_display[] = $obj;
						}
					}
					return $lines_display;
				} else {

					$this->error = "Error ".$this->db->lasterror();
					$this->errors[] = $this->error;

					dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
					
					var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);


					return -1;
				}
	 }



	 	/**
	 * load  inspection  from table worksho_inspectionsheet 
	 * @return 	int     				Return integer <0 if KO, 0 if not found, >0 if OK
	 * 
	 */

	 public function  fetchInspectionSheetHeaders($id)
	 {

		global $user;

		$lines_display = array();
		$sql = "SELECT DISTINCT
				wi.rowid as Ref, wi.inspectionname as inspectionname, wi.fk_categorie as `Tag/Categorie`,
				wi.description as `description`, wi.date_creation as Date_Created, hd.fk_inspectionheader as fk_inspectionheader,
				hd.fk_inspectionsheet as fk_inspectionsheet, hd.header_value as headervalue,
				wh.rowid as headerid, wh.label as label
				
				FROM " . MAIN_DB_PREFIX . "workshop_inspectionsheet as wi
				LEFT JOIN " . MAIN_DB_PREFIX . "inspectionheaders as hd ON wi.rowid = hd.fk_inspectionsheet
				LEFT JOIN " . MAIN_DB_PREFIX . "workshop_inspectionheader as wh ON hd.fk_inspectionheader = wh.rowid
				WHERE hd.fk_inspectionsheet = '" . $id . "'";




				// echo $sql;
		
				dol_syslog(__METHOD__, LOG_DEBUG);
				$resql = $this->db->query($sql);
				if ($resql) {

					$num = $this->db->num_rows($resql);

					if ($num) {
						while ($obj = $this->db->fetch_object($resql)) {
						$lines_display[] = $obj;
						}
					}

					return $lines_display;
				} else {

					$this->error = "Error ".$this->db->lasterror();
					$this->errors[] = $this->error;

					dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);

					var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
				

					return -1;
				}
	 }




	  	/**
	 * load  inspection  from table worksho_inspectionsheet 
	 * @return 	int     				Return integer <0 if KO, 0 if not found, >0 if OK
	 * 
	 */

	 public function  fetchInspectionSheetProducts($id)
	 {

		global $user;

		$lines_display = array();
		$sql = "SELECT DISTINCT
						wi.rowid as Ref, 
						wi.fk_inspectiontype as InspectionType, 
						wi.fk_categorie as `Tag/Categorie`,
						wi.fk_propal as Proposal, 
						wi.date_creation as Date_Created,
						hd.fk_inspectionsheet as fk_inspectionsheet, 
						hd.description as `description`, 
						hd.status as `status`, 
						hd.fk_product as fk_product, 
						wh.rowid as productid, 
						wh.price_ttc as price_ttc, 
						wh.price_min as price_min, 
						wh.label as label,
						cp.fk_product as productid,
						cp.fk_categorie as categorieid ,
						cg.rowid ,
						cg.label  as categorie
				FROM " . MAIN_DB_PREFIX . "workshop_inspectionsheet as wi
				LEFT JOIN " . MAIN_DB_PREFIX . "inspectionlines as hd ON wi.rowid = hd.fk_inspectionsheet
				LEFT JOIN " . MAIN_DB_PREFIX . "product as wh ON hd.fk_product = wh.rowid
				LEFT JOIN " . MAIN_DB_PREFIX . "categorie_product as cp ON   hd.fk_product = cp.fk_product
				LEFT JOIN " . MAIN_DB_PREFIX . "categorie as cg On cg.rowid = cp.fk_categorie
				WHERE hd.fk_inspectionsheet = '" . $id . "'";


				// echo $sql;
		
				dol_syslog(__METHOD__, LOG_DEBUG);
				$resql = $this->db->query($sql);
				if ($resql) {

					$num = $this->db->num_rows($resql);

					if ($num) {
						while ($obj = $this->db->fetch_object($resql)) {
						$lines_display[] = $obj;
						}
					}
					return $lines_display;
				} else {

					$this->error = "Error ".$this->db->lasterror();
					$this->errors[] = $this->error;

					dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
					var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
					
					return -1;
				}
	 }


	   	/**
	 * load  inspection  from table worksho_inspectionsheet 
	 * @return 	int     				Return integer <0 if KO, 0 if not found, >0 if OK
	 * 
	 */

	 public function  fetchInspectionSheetCauses($id)
	 {

		global $user;

		$lines_display = array();
		$sql = "SELECT 
						wi.rowid as Ref, 
						wi.fk_inspectiontype as InspectionType, 
						wi.fk_categorie as `Tag/Categorie`,
						wi.fk_propal as Proposal, 
						wi.date_creation as Date_Created,
						hd.fk_inspectioncause as fk_inspectioncause,
						hd.fk_inspectionsheet as fk_inspectionsheet, 
						hd.causevalue as causevalue, 
						wh.rowid as causeid, 
						wh.label as label
				FROM " . MAIN_DB_PREFIX . "workshop_inspectionsheet as wi
						LEFT JOIN " . MAIN_DB_PREFIX . "inspectioncauseoffailure as hd ON wi.rowid = hd.fk_inspectionsheet
						LEFT JOIN " . MAIN_DB_PREFIX . "workshop_causesoffailure as wh ON hd.fk_inspectioncause = wh.rowid
						WHERE hd.fk_inspectionsheet = '" . $id . "'";

		
				dol_syslog(__METHOD__, LOG_DEBUG);
				$resql = $this->db->query($sql);
				if ($resql) {

					$num = $this->db->num_rows($resql);

					if ($num) {
						while ($obj = $this->db->fetch_object($resql)) {
						$lines_display[] = $obj;
						}
					}
					return $lines_display;
				} else {

					$this->error = "Error ".$this->db->lasterror();
					$this->errors[] = $this->error;

					dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
					

					return -1;
				}
	 }

		/**
	 * Load object lines in memory from the database
	 *
	 * @param	int		$noextrafields	0=Default to load extrafields, 1=No extrafields
	 * @return 	int         			Return integer <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetchInspectionProductByTags(array $headerIds)
	{
		
		global $conf, $langs;
		$error = 0;
		$sql = 'SELECT DISTINCT
					p.rowid AS fk_product,
					p.label AS label,
					cp.fk_categorie AS category,
					ci.fk_inspectionheader AS inspectionheader
				FROM 
					llx_product AS p
				JOIN 
					llx_categorie_product AS cp ON p.rowid = cp.fk_product
				JOIN 
					llx_categorie_inspectionheader AS ci ON cp.fk_categorie = ci.fk_categorie
				JOIN 
					llx_workshop_inspectionheader AS wi ON ci.fk_inspectionheader = wi.rowid
				WHERE 
					ci.fk_inspectionheader IN ('.implode(',', $headerIds).')';


		// echo $sql;
		
		dol_syslog(__METHOD__, LOG_DEBUG);
		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			if ($num) {
				while ($obj = $this->db->fetch_object($resql)) {
					$this->lines_display[] = $obj;
				}
			}

			
			return $this->lines_display;
		} else {

			$this->error = "Error ".$this->db->lasterror();
			$this->errors[] = $this->error;

			dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
			
			var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);


			return -1;
		}
	}


			/**
	 * Load object lines in memory from the database
	 *
	 * @param	int		$noextrafields	0=Default to load extrafields, 1=No extrafields
	 * @return 	int         			Return integer <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetchInspectionsheetId($lineid)
	{
		
		global $conf, $langs;

		$error = 0;

		$sql = 'SELECT 
					fk_inspectionsheet , rowid,fk_propal
				FROM 
					llx_propaldet
				WHERE 
					rowid = '. $lineid. '';
		
		dol_syslog(__METHOD__, LOG_DEBUG);
		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			if ($num) {
				while ($obj = $this->db->fetch_object($resql)) {
					$this->lines_display[] = $obj;
				}
			}

			return $this->lines_display;
		} else {

			$this->error = "Error ".$this->db->lasterror();
			$this->errors[] = $this->error;

			dol_syslog(__METHOD__." ".implode(',', $this->errors), LOG_ERR);
			
			var_dump(__METHOD__." ".implode(',', $this->errors), LOG_ERR);

			return -1;
		}
	}

	/**
	 *    	Add a proposal line into database (linked to product/service or not)
	 *      The parameters are already supposed to be appropriate and with final values to the call
	 *      of this method. Also, for the VAT rate, it must have already been defined
	 *      by whose calling the method get_default_tva (societe_vendeuse, societe_acheteuse, '' product)
	 *      and desc must already have the right value (it's up to the caller to manage multilanguage)
	 *
	 * 		@param    	string		$desc				Description of line
	 * 		@param    	float		$pu_ht				Unit price
	 * 		@param    	float		$qty             	Quantity
	 * 		@param    	float		$txtva           	Force Vat rate, -1 for auto (Can contain the vat_src_code too with syntax '9.9 (CODE)')
	 * 		@param		float		$txlocaltax1		Local tax 1 rate (deprecated, use instead txtva with code inside)
	 *  	@param		float		$txlocaltax2		Local tax 2 rate (deprecated, use instead txtva with code inside)
	 *		@param    	int			$fk_product      	Product/Service ID predefined
	 * 		@param    	float		$remise_percent  	Pourcentage de remise de la ligne
	 * 		@param    	string		$price_base_type	HT or TTC
	 * 		@param    	float		$pu_ttc             Prix unitaire TTC
	 * 		@param    	int			$info_bits			Bits for type of lines
	 *      @param      int			$type               Type of line (0=product, 1=service). Not used if fk_product is defined, the type of product is used.
	 *      @param      int			$rang               Position of line
	 *      @param		int			$special_code		Special code (also used by externals modules!)
	 *      @param		int			$fk_parent_line		Id of parent line
	 *      @param		int			$fk_fournprice		Id supplier price
	 *      @param		int			$pa_ht				Buying price without tax
	 *      @param		string		$label				???
	 *		@param      int			$date_start       	Start date of the line
	 *		@param      int			$date_end         	End date of the line
	 *      @param		array		$array_options		extrafields array
	 * 		@param 		string		$fk_unit 			Code of the unit to use. Null to use the default one
	 *      @param		string		$origin				Depend on global conf MAIN_CREATEFROM_KEEP_LINE_ORIGIN_INFORMATION can be 'orderdet', 'propaldet'..., else 'order','propal,'....
	 *      @param		int			$origin_id			Depend on global conf MAIN_CREATEFROM_KEEP_LINE_ORIGIN_INFORMATION can be Id of origin object (aka line id), else object id
	 * 		@param		double		$pu_ht_devise		Unit price in currency
	 * 		@param		int    		$fk_remise_except	Id discount if line is from a discount
	 *  	@param		int			$noupdateafterinsertline	No update after insert of line
	 *    	@return    	int         	    			>0 if OK, <0 if KO
	 *    	@see       	add_product()
	 */

	 public function addInspectionLineItem($desc, $pu_ht, $qty, $txtva, $txlocaltax1 = 0.0, $txlocaltax2 = 0.0, $fk_product = 0, $remise_percent = 0.0, $price_base_type = 'HT', $pu_ttc = 0.0, $info_bits = 0, $type = '0', $rang = -1, $special_code = 0, $fk_parent_line = 0, $fk_fournprice = 0, $pa_ht = 0, $label = '', $date_start = '', $date_end = '', $array_options = 0, $fk_unit = null, $origin = '', $origin_id = 0, $pu_ht_devise = 0, $fk_remise_except = 0, $noupdateafterinsertline = 0, $fk_propal , Propal $propale,$fk_inspectionsheet )
	 {
 

		 global $mysoc, $conf, $langs;
 
 

		 dol_syslog(get_class($propale)."::addline propalid=".($fk_propal ? $fk_propal : $fk_propal).", desc=$desc, pu_ht=$pu_ht, qty=$qty, txtva=$txtva, fk_product=$fk_product, remise_except=$remise_percent, price_base_type=$price_base_type, pu_ttc=$pu_ttc, info_bits=$info_bits, type=$type, fk_remise_except=".$fk_remise_except);
		 
			 include_once DOL_DOCUMENT_ROOT.'/core/lib/price.lib.php';
 
			 // Clean parameters
			 if (empty($remise_percent)) {
				 $remise_percent = 0;
			 }
			 if (empty($qty)) {
				 $qty = 0;
			 }
			 if (empty($info_bits)) {
				 $info_bits = 0;
			 }
			 if (empty($rang)) {
				 $rang = 0;
			 }
			 if (empty($fk_parent_line) || $fk_parent_line < 0) {
				 $fk_parent_line = 0;
			 }
 
 
			 $remise_percent = price2num($remise_percent);
			 $qty = price2num($qty);
			 $pu_ht = price2num($pu_ht);
			 $pu_ht_devise = price2num($pu_ht_devise);
			 $pu_ttc = price2num($pu_ttc);
 
			 if (!preg_match('/\((.*)\)/', $txtva)) {
				 $txtva = price2num($txtva); // $txtva can have format '5,1' or '5.1' or '5.1(XXX)', we must clean only if '5,1'
			 }
			 $txlocaltax1 = price2num($txlocaltax1);
			 $txlocaltax2 = price2num($txlocaltax2);
			 $pa_ht = price2num($pa_ht);
			 if ($price_base_type == 'HT') {
				 $pu = $pu_ht;
			 } else {
				 $pu = $pu_ttc;
			 }
 
			 // if (!empty($fk_product) && $fk_product > 0) {
				 
			 // 	$product = new Product($this->db);
			 // 	$result = $product->fetch($fk_product);
			 // 	$product_type = $product->type;
 
			 // 	if (!empty($conf->global->STOCK_MUST_BE_ENOUGH_FOR_PROPOSAL) && $product_type == 0 && $product->stock_reel < $qty) {
			 // 		$langs->load("errors");
			 // 		$this->error = $langs->trans('ErrorStockIsNotEnoughToAddProductOnProposal', $product->ref);
			 // 		$this->db->rollback();
			 // 		return -3;
			 // 	}
			 // }
 
 
 
			 $tabprice = calcul_price_total($qty, $pu, $remise_percent, $txtva, $txlocaltax1, $txlocaltax2, 0, $price_base_type, $info_bits, $product_type, $mysoc, $localtaxes_type, 100, $this->multicurrency_tx, $pu_ht_devise);
 
			 $total_ht  = $tabprice[0];
			 $total_tva = $tabprice[1];
			 $total_ttc = $tabprice[2];
			 $total_localtax1 = $tabprice[9];
			 $total_localtax2 = $tabprice[10];
			 $pu_ht  = $tabprice[3];
			 $pu_tva = $tabprice[4];
			 $pu_ttc = $tabprice[5];
 
		 
 
 
			 // MultiCurrency
			 $multicurrency_total_ht  = $tabprice[16];
			 $multicurrency_total_tva = $tabprice[17];
			 $multicurrency_total_ttc = $tabprice[18];
			 $pu_ht_devise = $tabprice[19];
		 
	
 
			 // Anciens indicateurs: $price, $remise (a ne plus utiliser)
			 $price = $pu;
			 $remise = 0;


			 if ($remise_percent > 0) {
				 $remise = round(($pu * $remise_percent / 100), 2);
				 $price = $pu - $remise;
			 }
 
						 // Insert line
						 $this->line = new PropaleLigne($this->db);			 
						 $this->line->fk_propal = $fk_propal;
						 $this->line->label = $label;
						 $this->line->desc = $desc;
						 $this->line->qty = $qty;
			 
						 $this->line->vat_src_code = $vat_src_code;
						 $this->line->tva_tx = $txtva;
						 $this->line->localtax1_tx = ($total_localtax1 ? $localtaxes_type[1] : 0);
						 $this->line->localtax2_tx = ($total_localtax2 ? $localtaxes_type[3] : 0);
						 $this->line->localtax1_type = empty($localtaxes_type[0]) ? '' : $localtaxes_type[0];
						 $this->line->localtax2_type = empty($localtaxes_type[2]) ? '' : $localtaxes_type[2];
						 $this->line->fk_product = $fk_product;
						 $this->line->product_type = $type;
						 $this->line->fk_remise_except = $fk_remise_except;
						 $this->line->remise_percent = $remise_percent;
						 $this->line->subprice = $pu_ht;
						 $this->line->rang = $ranktouse;
						 $this->line->info_bits = $info_bits;
						 $this->line->total_ht = $total_ht;
						 $this->line->total_tva = $total_tva;
						 $this->line->total_localtax1 = $total_localtax1;
						 $this->line->total_localtax2 = $total_localtax2;
						 $this->line->total_ttc = $total_ttc;
						 $this->line->special_code = $special_code;
						 $this->line->fk_parent_line = $fk_parent_line;
						 $this->line->fk_unit = $fk_unit;
			 
						 $this->line->date_start = $date_start;
						 $this->line->date_end = $date_end;
			 
						 $this->line->fk_fournprice = $fk_fournprice;
						 $this->line->pa_ht = $pa_ht;
			 
						 $this->line->origin_id = $origin_id;
						 $this->line->origin = $origin;
 
			 
						 // Multicurrency
						 $this->line->fk_multicurrency = $fk_multicurrency;
						 $this->line->multicurrency_code = $multicurrency_code;
						 $this->line->multicurrency_subprice		= $pu_ht_devise;
						 $this->line->multicurrency_total_ht 	= $multicurrency_total_ht;
						 $this->line->multicurrency_total_tva 	= $multicurrency_total_tva;
						 $this->line->multicurrency_total_ttc 	= $multicurrency_total_ttc;

			 
			 
						 
					 
			 
						 // Mise en option de la ligne
						 if (empty($qty) && empty($special_code)) {
							 $this->line->special_code = 3;
						 }
			 
						 // TODO deprecated
						 $this->line->price = $price;
			 
						 if (is_array($array_options) && count($array_options) > 0) {
							 $this->line->array_options = $array_options;
						 }
			 
						 $result = $this->line->insert($fk_inspectionsheet);
						 if ($result > 0) {
							

							 
							 // Reorder if child line
							 if (!empty($fk_parent_line)) {
			 
								 
								 $this->line_order(true, 'DESC');
							 } elseif ($ranktouse > 0 && $ranktouse <= count($this->lines)) { // Update all rank of all other lines
								 
								 $linecount = count($this->lines);
								 for ($ii = $ranktouse; $ii <= $linecount; $ii++) {
									 $this->updateRangOfLine($this->lines[$ii - 1]->id, $ii + 1);
								 }
							 }
			 
							 // Mise a jour informations denormalisees au niveau de la propale meme
							 if (empty($noupdateafterinsertline)) {

								
								 $result = $propale->update_price(1, 'auto', 0, $mysoc);

								  // This method is designed to add line from user input so total calculation must be done using 'auto' mode.
							 }
			 
							 if ($result > 0) {
								

								 $this->db->commit();

								 return $this->line->id;
							 } else {

								
								 $this->error = $this->db->error();
								 $this->db->rollback();
								 return -1;
							 }
						 } else {
							 $this->error = $this->line->error;
							 $this->errors = $this->line->errors;
							 $this->db->rollback();
							 return -2;
						 }

					
	 }
 

	/**
	 * Load list of objects in memory from the database.
	 * Using a fetchAll() with limit = 0 is a very bad practice. Instead try to forge yourself an optimized SQL request with
	 * your own loop with start and stop pagination.
	 *
	 * @param  string      	$sortorder    	Sort Order
	 * @param  string      	$sortfield    	Sort field
	 * @param  int         	$limit        	Limit the number of lines returned
	 * @param  int         	$offset       	Offset
	 * @param  string		$filter       	Filter as an Universal Search string.
	 * 										Example: '((client:=:1) OR ((client:>=:2) AND (client:<=:3))) AND (client:!=:8) AND (nom:like:'a%')'
	 * @param  string      	$filtermode   	Filter mode (AND or OR)
	 * @return array|int                 	int <0 if KO, array of pages if OK
	 */
	public function fetchAll($sortorder = '', $sortfield = '', $limit = 1000, $offset = 0, string $filter = '', $filtermode = 'AND')
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$records = array();

		$sql = "SELECT ";
		$sql .= $this->getFieldList('t');
		$sql .= " FROM ".$this->db->prefix().$this->table_element." as t";
		if (isset($this->isextrafieldmanaged) && $this->isextrafieldmanaged == 1) {
			$sql .= " LEFT JOIN ".$this->db->prefix().$this->table_element."_extrafields as te ON te.fk_object = t.rowid";
		}
		if (isset($this->ismultientitymanaged) && $this->ismultientitymanaged == 1) {
			$sql .= " WHERE t.entity IN (".getEntity($this->element).")";
		} else {
			$sql .= " WHERE 1 = 1";
		}
		// Manage filter
		/* We keep this part of code that is still used by a lot of old class. The 'else" shows how to switch to Universal Search filters
		$sqlwhere = array();
		if (is_array($filter) && count($filter) > 0) {
			dol_syslog("Warning: Use of an array  as filter is now forbidden and deprecated. Use an universal SQL filter string instead", LOG_WARNING);
			foreach ($filter as $key => $value) {
				$columnName = preg_replace('/^t\./', '', $key);
				if ($key === 'customsql') {
					// Never use 'customsql' with a value from a user input since it is injected as is. The value must be hard coded.
					$sqlwhere[] = $value;
					continue;
				} elseif (isset($this->fields[$columnName])) {
					$type = $this->fields[$columnName]['type'];
					if (preg_match('/^integer/', $type)) {
						if (is_int($value)) {
							// single value
							$sqlwhere[] = $key . " = " . intval($value);
						} elseif (is_array($value)) {
							if (empty($value)) {
								continue;
							}
							$sqlwhere[] = $key . ' IN (' . $this->db->sanitize(implode(',', array_map('intval', $value))) . ')';
						}
						continue;
					} elseif (in_array($type, array('date', 'datetime', 'timestamp'))) {
						$sqlwhere[] = $key . " = '" . $this->db->idate($value) . "'";
						continue;
					}
				}

				// when the $key doesn't fall into the previously handled categories, we do as if the column were a varchar/text
				if (is_array($value) && count($value)) {
					$value = implode(',', array_map(function ($v) {
						return "'" . $this->db->sanitize($this->db->escape($v)) . "'";
					}, $value));
					$sqlwhere[] = $key . ' IN (' . $this->db->sanitize($value, true) . ')';
				} elseif (is_scalar($value)) {
					if (strpos($value, '%') === false) {
						$sqlwhere[] = $key . " = '" . $this->db->sanitize($this->db->escape($value)) . "'";
					} else {
						$sqlwhere[] = $key . " LIKE '%" . $this->db->escape($this->db->escapeforlike($value)) . "%'";
					}
				}
			}
		} else { */
		$errormessage = '';
		$sql .= forgeSQLFromUniversalSearchCriteria($filter, $errormessage);
		if ($errormessage) {
			$this->errors[] = $errormessage;
			dol_syslog(__METHOD__.' '.join(',', $this->errors), LOG_ERR);

			return -1;
		}
		/*}
		if (count($sqlwhere) > 0) {
			$sql .= " AND (".implode(" ".$filtermode." ", $sqlwhere).")";
		}*/

		if (!empty($sortfield)) {
			$sql .= $this->db->order($sortfield, $sortorder);
		}
		if (!empty($limit)) {
			$sql .= $this->db->plimit($limit, $offset);
		}

		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			$i = 0;
			while ($i < ($limit ? min($limit, $num) : $num)) {
				$obj = $this->db->fetch_object($resql);

				$record = new self($this->db);
				$record->setVarsFromFetchObj($obj);

				$records[$record->id] = $record;

				$i++;
			}
			$this->db->free($resql);

			return $records;
		} else {
			$this->errors[] = 'Error '.$this->db->lasterror();
			dol_syslog(__METHOD__.' '.join(',', $this->errors), LOG_ERR);

			return -1;
		}
	}

	/**
	 * Update object into database
	 *
	 * @param  User $user      User that modifies
	 * @param  int 	$notrigger 0=launch triggers after, 1=disable triggers
	 * @return int             Return integer <0 if KO, >0 if OK
	 */
	public function update(User $user, $notrigger = 0)
	{
		return $this->updateCommon($user, $notrigger);
	}

	/**
	 * Delete object in database
	 *
	 * @param User $user       	User that deletes
	 * @param int 	$notrigger  0=launch triggers, 1=disable triggers
	 * @return int             	Return integer <0 if KO, >0 if OK
	 */
	public function delete(User $user, $notrigger = 0)
	{
		return $this->deleteCommon($user, $notrigger);
		//return $this->deleteCommon($user, $notrigger, 1);
	}

	/**
	 *  Delete a line of object in database
	 *
	 *	@param  User	$user       User that delete
	 *  @param	int		$idline		Id of line to delete
	 *  @param 	int 	$notrigger  0=launch triggers after, 1=disable triggers
	 *  @return int         		>0 if OK, <0 if KO
	 */
	public function deleteLine(User $user, $idline, $notrigger = 0)
	{
		if ($this->status < 0) {
			$this->error = 'ErrorDeleteLineNotAllowedByObjectStatus';
			return -2;
		}

		return $this->deleteLineCommon($user, $idline, $notrigger);
	}


	/**
	 *	Validate object
	 *
	 *	@param		User	$user     		User making status change
	 *  @param		int		$notrigger		1=Does not execute triggers, 0= execute triggers
	 *	@return  	int						Return integer <=0 if OK, 0=Nothing done, >0 if KO
	 */
	public function validate($user, $notrigger = 0)
	{
		global $conf, $langs;

		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';

		$error = 0;

		// Protection
		if ($this->status == self::STATUS_VALIDATED) {
			dol_syslog(get_class($this)."::validate action abandoned: already validated", LOG_WARNING);
			return 0;
		}

		/* if (! ((!getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop', 'inspectionsheet', 'write'))
		 || (getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop', 'inspectionsheet_advance', 'validate')))
		 {
		 $this->error='NotEnoughPermissions';
		 dol_syslog(get_class($this)."::valid ".$this->error, LOG_ERR);
		 return -1;
		 }*/

		$now = dol_now();

		$this->db->begin();

		// Define new ref
		if (!$error && (preg_match('/^[\(]?PROV/i', $this->ref) || empty($this->ref))) { // empty should not happened, but when it occurs, the test save life
			$num = $this->getNextNumRef();
		} else {
			$num = $this->ref;
		}
		$this->newref = $num;

		if (!empty($num)) {
			// Validate
			$sql = "UPDATE ".MAIN_DB_PREFIX.$this->table_element;
			$sql .= " SET ref = '".$this->db->escape($num)."',";
			$sql .= " status = ".self::STATUS_VALIDATED;
			if (!empty($this->fields['date_validation'])) {
				$sql .= ", date_validation = '".$this->db->idate($now)."'";
			}
			if (!empty($this->fields['fk_user_valid'])) {
				$sql .= ", fk_user_valid = ".((int) $user->id);
			}
			$sql .= " WHERE rowid = ".((int) $this->id);

			dol_syslog(get_class($this)."::validate()", LOG_DEBUG);
			$resql = $this->db->query($sql);
			if (!$resql) {
				dol_print_error($this->db);
				$this->error = $this->db->lasterror();
				$error++;
			}

			if (!$error && !$notrigger) {
				// Call trigger
				$result = $this->call_trigger('MYOBJECT_VALIDATE', $user);
				if ($result < 0) {
					$error++;
				}
				// End call triggers
			}
		}

		if (!$error) {
			$this->oldref = $this->ref;

			// Rename directory if dir was a temporary ref
			if (preg_match('/^[\(]?PROV/i', $this->ref)) {
				// Now we rename also files into index
				$sql = 'UPDATE '.MAIN_DB_PREFIX."ecm_files set filename = CONCAT('".$this->db->escape($this->newref)."', SUBSTR(filename, ".(strlen($this->ref) + 1).")), filepath = 'inspectionsheet/".$this->db->escape($this->newref)."'";
				$sql .= " WHERE filename LIKE '".$this->db->escape($this->ref)."%' AND filepath = 'inspectionsheet/".$this->db->escape($this->ref)."' and entity = ".$conf->entity;
				$resql = $this->db->query($sql);
				if (!$resql) {
					$error++;
					$this->error = $this->db->lasterror();
				}
				$sql = 'UPDATE '.MAIN_DB_PREFIX."ecm_files set filepath = 'inspectionsheet/".$this->db->escape($this->newref)."'";
				$sql .= " WHERE filepath = 'inspectionsheet/".$this->db->escape($this->ref)."' and entity = ".$conf->entity;
				$resql = $this->db->query($sql);
				if (!$resql) {
					$error++;
					$this->error = $this->db->lasterror();
				}

				// We rename directory ($this->ref = old ref, $num = new ref) in order not to lose the attachments
				$oldref = dol_sanitizeFileName($this->ref);
				$newref = dol_sanitizeFileName($num);
				$dirsource = $conf->workshop->dir_output.'/inspectionsheet/'.$oldref;
				$dirdest = $conf->workshop->dir_output.'/inspectionsheet/'.$newref;
				if (!$error && file_exists($dirsource)) {
					dol_syslog(get_class($this)."::validate() rename dir ".$dirsource." into ".$dirdest);

					if (@rename($dirsource, $dirdest)) {
						dol_syslog("Rename ok");
						// Rename docs starting with $oldref with $newref
						$listoffiles = dol_dir_list($conf->workshop->dir_output.'/inspectionsheet/'.$newref, 'files', 1, '^'.preg_quote($oldref, '/'));
						foreach ($listoffiles as $fileentry) {
							$dirsource = $fileentry['name'];
							$dirdest = preg_replace('/^'.preg_quote($oldref, '/').'/', $newref, $dirsource);
							$dirsource = $fileentry['path'].'/'.$dirsource;
							$dirdest = $fileentry['path'].'/'.$dirdest;
							@rename($dirsource, $dirdest);
						}
					}
				}
			}
		}

		// Set new ref and current status
		if (!$error) {
			$this->ref = $num;
			$this->status = self::STATUS_VALIDATED;
		}

		if (!$error) {
			$this->db->commit();
			return 1;
		} else {
			$this->db->rollback();
			return -1;
		}
	}


	/**
	 *	Set draft status
	 *
	 *	@param	User	$user			Object user that modify
	 *  @param	int		$notrigger		1=Does not execute triggers, 0=Execute triggers
	 *	@return	int						Return integer <0 if KO, >0 if OK
	 */
	public function setDraft($user, $notrigger = 0)
	{
		// Protection
		if ($this->status <= self::STATUS_DRAFT) {
			return 0;
		}

		/* if (! ((!getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop','write'))
		 || (getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop','workshop_advance','validate'))))
		 {
		 $this->error='Permission denied';
		 return -1;
		 }*/

		return $this->setStatusCommon($user, self::STATUS_DRAFT, $notrigger, 'WORKSHOP_MYOBJECT_UNVALIDATE');
	}

	/**
	 *	Set cancel status
	 *
	 *	@param	User	$user			Object user that modify
	 *  @param	int		$notrigger		1=Does not execute triggers, 0=Execute triggers
	 *	@return	int						Return integer <0 if KO, 0=Nothing done, >0 if OK
	 */
	public function cancel($user, $notrigger = 0)
	{
		// Protection
		if ($this->status != self::STATUS_VALIDATED) {
			return 0;
		}

		/* if (! ((!getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop','write'))
		 || (getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop','workshop_advance','validate'))))
		 {
		 $this->error='Permission denied';
		 return -1;
		 }*/

		return $this->setStatusCommon($user, self::STATUS_CANCELED, $notrigger, 'WORKSHOP_MYOBJECT_CANCEL');
	}

	/**
	 *	Set back to validated status
	 *
	 *	@param	User	$user			Object user that modify
	 *  @param	int		$notrigger		1=Does not execute triggers, 0=Execute triggers
	 *	@return	int						Return integer <0 if KO, 0=Nothing done, >0 if OK
	 */
	public function reopen($user, $notrigger = 0)
	{
		// Protection
		if ($this->status == self::STATUS_VALIDATED) {
			return 0;
		}

		/*if (! ((!getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop','write'))
		 || (getDolGlobalInt('MAIN_USE_ADVANCED_PERMS') && $user->hasRight('workshop','workshop_advance','validate'))))
		 {
		 $this->error='Permission denied';
		 return -1;
		 }*/

		return $this->setStatusCommon($user, self::STATUS_VALIDATED, $notrigger, 'WORKSHOP_MYOBJECT_REOPEN');
	}

	/**
	 * getTooltipContentArray
	 *
	 * @param 	array 	$params 	Params to construct tooltip data
	 * @since 	v18
	 * @return 	array
	 */
	public function getTooltipContentArray($params)
	{
		global $langs;

		$datas = [];

		if (getDolGlobalInt('MAIN_OPTIMIZEFORTEXTBROWSER')) {
			return ['optimize' => $langs->trans("ShowInspectionsheet")];
		}
		$datas['picto'] = img_picto('', $this->picto).' <u>'.$langs->trans("Inspectionsheet").'</u>';
		if (isset($this->status)) {
			$datas['picto'] .= ' '.$this->getLibStatut(5);
		}
		if (property_exists($this, 'ref')) {
			$datas['ref'] = '<br><b>'.$langs->trans('Ref').':</b> '.$this->ref;
		}
		if (property_exists($this, 'label')) {
			$datas['ref'] = '<br>'.$langs->trans('Label').':</b> '.$this->label;
		}

		return $datas;
	}

	/**
	 *  Return a link to the object card (with optionally the picto)
	 *
	 *  @param  int     $withpicto                  Include picto in link (0=No picto, 1=Include picto into link, 2=Only picto)
	 *  @param  string  $option                     On what the link point to ('nolink', ...)
	 *  @param  int     $notooltip                  1=Disable tooltip
	 *  @param  string  $morecss                    Add more css on link
	 *  @param  int     $save_lastsearch_value      -1=Auto, 0=No save of lastsearch_values when clicking, 1=Save lastsearch_values whenclicking
	 *  @return	string                              String with URL
	 */
	public function getNomUrl($withpicto = 0, $option = '', $notooltip = 0, $morecss = '', $save_lastsearch_value = -1)
	{
		global $conf, $langs, $hookmanager;

		if (!empty($conf->dol_no_mouse_hover)) {
			$notooltip = 1; // Force disable tooltips
		}

		$result = '';
		$params = [
			'id' => $this->id,
			'objecttype' => $this->element.($this->module ? '@'.$this->module : ''),
			'option' => $option,
		];
		$classfortooltip = 'classfortooltip';
		$dataparams = '';
		if (getDolGlobalInt('MAIN_ENABLE_AJAX_TOOLTIP')) {
			$classfortooltip = 'classforajaxtooltip';
			$dataparams = ' data-params="'.dol_escape_htmltag(json_encode($params)).'"';
			$label = '';
		} else {
			$label = implode($this->getTooltipContentArray($params));
		}

		$url = dol_buildpath('/workshop/inspectionsheet_card.php', 1).'?id='.$this->id;

		if ($option !== 'nolink') {
			// Add param to save lastsearch_values or not
			$add_save_lastsearch_values = ($save_lastsearch_value == 1 ? 1 : 0);
			if ($save_lastsearch_value == -1 && isset($_SERVER["PHP_SELF"]) && preg_match('/list\.php/', $_SERVER["PHP_SELF"])) {
				$add_save_lastsearch_values = 1;
			}
			if ($url && $add_save_lastsearch_values) {
				$url .= '&save_lastsearch_values=1';
			}
		}

		$linkclose = '';
		if (empty($notooltip)) {
			if (getDolGlobalInt('MAIN_OPTIMIZEFORTEXTBROWSER')) {
				$label = $langs->trans("ShowInspectionsheet");
				$linkclose .= ' alt="'.dol_escape_htmltag($label, 1).'"';
			}
			$linkclose .= ($label ? ' title="'.dol_escape_htmltag($label, 1).'"' : ' title="tocomplete"');
			$linkclose .= $dataparams.' class="'.$classfortooltip.($morecss ? ' '.$morecss : '').'"';
		} else {
			$linkclose = ($morecss ? ' class="'.$morecss.'"' : '');
		}

		if ($option == 'nolink' || empty($url)) {
			$linkstart = '<span';
		} else {
			$linkstart = '<a href="'.$url.'"';
		}
		$linkstart .= $linkclose.'>';
		if ($option == 'nolink' || empty($url)) {
			$linkend = '</span>';
		} else {
			$linkend = '</a>';
		}

		$result .= $linkstart;

		if (empty($this->showphoto_on_popup)) {
			if ($withpicto) {
				$result .= img_object(($notooltip ? '' : $label), ($this->picto ? $this->picto : 'generic'), (($withpicto != 2) ? 'class="paddingright"' : ''), 0, 0, $notooltip ? 0 : 1);
			}
		} else {
			if ($withpicto) {
				require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';

				list($class, $module) = explode('@', $this->picto);
				$upload_dir = $conf->$module->multidir_output[$conf->entity]."/$class/".dol_sanitizeFileName($this->ref);
				$filearray = dol_dir_list($upload_dir, "files");
				$filename = $filearray[0]['name'];
				if (!empty($filename)) {
					$pospoint = strpos($filearray[0]['name'], '.');

					$pathtophoto = $class.'/'.$this->ref.'/thumbs/'.substr($filename, 0, $pospoint).'_mini'.substr($filename, $pospoint);
					if (!getDolGlobalString(strtoupper($module.'_'.$class).'_FORMATLISTPHOTOSASUSERS')) {
						$result .= '<div class="floatleft inline-block valignmiddle divphotoref"><div class="photoref"><img class="photo'.$module.'" alt="No photo" border="0" src="'.DOL_URL_ROOT.'/viewimage.php?modulepart='.$module.'&entity='.$conf->entity.'&file='.urlencode($pathtophoto).'"></div></div>';
					} else {
						$result .= '<div class="floatleft inline-block valignmiddle divphotoref"><img class="photouserphoto userphoto" alt="No photo" border="0" src="'.DOL_URL_ROOT.'/viewimage.php?modulepart='.$module.'&entity='.$conf->entity.'&file='.urlencode($pathtophoto).'"></div>';
					}

					$result .= '</div>';
				} else {
					$result .= img_object(($notooltip ? '' : $label), ($this->picto ? $this->picto : 'generic'), ($notooltip ? (($withpicto != 2) ? 'class="paddingright"' : '') : 'class="'.(($withpicto != 2) ? 'paddingright ' : '').'"'), 0, 0, $notooltip ? 0 : 1);
				}
			}
		}

		if ($withpicto != 2) {
			$result .= $this->ref;
		}

		$result .= $linkend;
		//if ($withpicto != 2) $result.=(($addlabel && $this->label) ? $sep . dol_trunc($this->label, ($addlabel > 1 ? $addlabel : 0)) : '');

		global $action, $hookmanager;
		$hookmanager->initHooks(array($this->element.'dao'));
		$parameters = array('id' => $this->id, 'getnomurl' => &$result);
		$reshook = $hookmanager->executeHooks('getNomUrl', $parameters, $this, $action); // Note that $action and $object may have been modified by some hooks
		if ($reshook > 0) {
			$result = $hookmanager->resPrint;
		} else {
			$result .= $hookmanager->resPrint;
		}

		return $result;
	}

	/**
	 *	Return a thumb for kanban views
	 *
	 *	@param      string	    $option                 Where point the link (0=> main card, 1,2 => shipment, 'nolink'=>No link)
	 *  @param		array		$arraydata				Array of data
	 *  @return		string								HTML Code for Kanban thumb.
	 */
	public function getKanbanView($option = '', $arraydata = null)
	{
		global $conf, $langs;

		$selected = (empty($arraydata['selected']) ? 0 : $arraydata['selected']);

		$return = '<div class="box-flex-item box-flex-grow-zero">';
		$return .= '<div class="info-box info-box-sm">';
		$return .= '<span class="info-box-icon bg-infobox-action">';
		$return .= img_picto('', $this->picto);
		$return .= '</span>';
		$return .= '<div class="info-box-content">';
		$return .= '<span class="info-box-ref inline-block tdoverflowmax150 valignmiddle">'.(method_exists($this, 'getNomUrl') ? $this->getNomUrl() : $this->ref).'</span>';
		if ($selected >= 0) {
			$return .= '<input id="cb'.$this->id.'" class="flat checkforselect fright" type="checkbox" name="toselect[]" value="'.$this->id.'"'.($selected ? ' checked="checked"' : '').'>';
		}
		if (property_exists($this, 'label')) {
			$return .= ' <div class="inline-block opacitymedium valignmiddle tdoverflowmax100">'.$this->label.'</div>';
		}
		if (property_exists($this, 'thirdparty') && is_object($this->thirdparty)) {
			$return .= '<br><div class="info-box-ref tdoverflowmax150">'.$this->thirdparty->getNomUrl(1).'</div>';
		}
		if (property_exists($this, 'amount')) {
			$return .= '<br>';
			$return .= '<span class="info-box-label amount">'.price($this->amount, 0, $langs, 1, -1, -1, $conf->currency).'</span>';
		}
		if (method_exists($this, 'getLibStatut')) {
			$return .= '<br><div class="info-box-status">'.$this->getLibStatut(3).'</div>';
		}
		$return .= '</div>';
		$return .= '</div>';
		$return .= '</div>';

		return $return;
	}

	/**
	 *  Return the label of the status
	 *
	 *  @param  int		$mode          0=long label, 1=short label, 2=Picto + short label, 3=Picto, 4=Picto + long label, 5=Short label + Picto, 6=Long label + Picto
	 *  @return	string 			       Label of status
	 */
	public function getLabelStatus($mode = 0)
	{
		return $this->LibStatut($this->status, $mode);
	}

	/**
	 *  Return the label of the status
	 *
	 *  @param  int		$mode          0=long label, 1=short label, 2=Picto + short label, 3=Picto, 4=Picto + long label, 5=Short label + Picto, 6=Long label + Picto
	 *  @return	string 			       Label of status
	 */
	public function getLibStatut($mode = 0)
	{
		return $this->LibStatut($this->status, $mode);
	}

	// phpcs:disable PEAR.NamingConventions.ValidFunctionName.ScopeNotCamelCaps
	/**
	 *  Return the label of a given status
	 *
	 *  @param	int		$status        Id status
	 *  @param  int		$mode          0=long label, 1=short label, 2=Picto + short label, 3=Picto, 4=Picto + long label, 5=Short label + Picto, 6=Long label + Picto
	 *  @return string 			       Label of status
	 */
	public function LibStatut($status, $mode = 0)
	{
		// phpcs:enable
		if (is_null($status)) {
			return '';
		}

		if (empty($this->labelStatus) || empty($this->labelStatusShort)) {
			global $langs;
			//$langs->load("workshop@workshop");
			$this->labelStatus[self::STATUS_DRAFT] = $langs->transnoentitiesnoconv('Draft');
			$this->labelStatus[self::STATUS_VALIDATED] = $langs->transnoentitiesnoconv('Enabled');
			$this->labelStatus[self::STATUS_CANCELED] = $langs->transnoentitiesnoconv('Disabled');
			$this->labelStatusShort[self::STATUS_DRAFT] = $langs->transnoentitiesnoconv('Draft');
			$this->labelStatusShort[self::STATUS_VALIDATED] = $langs->transnoentitiesnoconv('Enabled');
			$this->labelStatusShort[self::STATUS_CANCELED] = $langs->transnoentitiesnoconv('Disabled');
		}

		$statusType = 'status'.$status;
		//if ($status == self::STATUS_VALIDATED) $statusType = 'status1';
		if ($status == self::STATUS_CANCELED) {
			$statusType = 'status6';
		}

		return dolGetStatus($this->labelStatus[$status], $this->labelStatusShort[$status], '', $statusType, $mode);
	}

	/**
	 *	Load the info information in the object
	 *
	 *	@param  int		$id       Id of object
	 *	@return	void
	 */
	public function info($id)
	{
		$sql = "SELECT rowid,";
		$sql .= " date_creation as datec, tms as datem";
		if (!empty($this->fields['date_validation'])) {
			$sql .= ", date_validation as datev";
		}
		if (!empty($this->fields['fk_user_creat'])) {
			$sql .= ", fk_user_creat";
		}
		if (!empty($this->fields['fk_user_modif'])) {
			$sql .= ", fk_user_modif";
		}
		if (!empty($this->fields['fk_user_valid'])) {
			$sql .= ", fk_user_valid";
		}
		$sql .= " FROM ".MAIN_DB_PREFIX.$this->table_element." as t";
		$sql .= " WHERE t.rowid = ".((int) $id);

		$result = $this->db->query($sql);
		if ($result) {
			if ($this->db->num_rows($result)) {
				$obj = $this->db->fetch_object($result);

				$this->id = $obj->rowid;

				if (!empty($this->fields['fk_user_creat'])) {
					$this->user_creation_id = $obj->fk_user_creat;
				}
				if (!empty($this->fields['fk_user_modif'])) {
					$this->user_modification_id = $obj->fk_user_modif;
				}
				if (!empty($this->fields['fk_user_valid'])) {
					$this->user_validation_id = $obj->fk_user_valid;
				}
				$this->date_creation     = $this->db->jdate($obj->datec);
				$this->date_modification = empty($obj->datem) ? '' : $this->db->jdate($obj->datem);
				if (!empty($obj->datev)) {
					$this->date_validation   = empty($obj->datev) ? '' : $this->db->jdate($obj->datev);
				}
			}

			$this->db->free($result);
		} else {
			dol_print_error($this->db);
		}
	}

	/**
	 * Initialise object with example values
	 * Id must be 0 if object instance is a specimen
	 *
	 * @return void
	 */
	public function initAsSpecimen()
	{
		// Set here init that are not commonf fields
		// $this->property1 = ...
		// $this->property2 = ...

		$this->initAsSpecimenCommon();
	}

	/**
	 * 	Create an array of lines
	 *
	 * 	@return array|int		array of lines if OK, <0 if KO
	 */
	public function getLinesArray()
	{
		$this->lines = array();

		$objectline = new InspectionsheetLine($this->db);
		$result = $objectline->fetchAll('ASC', 'position', 0, 0, array('customsql'=>'fk_inspectionsheet = '.((int) $this->id)));

		if (is_numeric($result)) {
			$this->setErrorsFromObject($objectline);
			return $result;
		} else {
			$this->lines = $result;
			return $this->lines;
		}
	}

	/**
	 *  Returns the reference to the following non used object depending on the active numbering module.
	 *
	 *  @return string      		Object free reference
	 */
	public function getNextNumRef()
	{
		global $langs, $conf;
		$langs->load("workshop@workshop");

		if (!getDolGlobalString('WORKSHOP_MYOBJECT_ADDON')) {
			$conf->global->WORKSHOP_MYOBJECT_ADDON = 'mod_inspectionsheet_standard';
		}

		if (getDolGlobalString('WORKSHOP_MYOBJECT_ADDON')) {
			$mybool = false;

			$file = getDolGlobalString('WORKSHOP_MYOBJECT_ADDON').".php";
			$classname = getDolGlobalString('WORKSHOP_MYOBJECT_ADDON');

			// Include file with class
			$dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);
			foreach ($dirmodels as $reldir) {
				$dir = dol_buildpath($reldir."core/modules/workshop/");

				// Load file with numbering class (if found)
				$mybool |= @include_once $dir.$file;
			}

			if ($mybool === false) {
				dol_print_error(null, "Failed to include file ".$file);
				return '';
			}

			if (class_exists($classname)) {
				$obj = new $classname();
				$numref = $obj->getNextValue($this);

				if ($numref != '' && $numref != '-1') {
					return $numref;
				} else {
					$this->error = $obj->error;
					//dol_print_error($this->db,get_class($this)."::getNextNumRef ".$obj->error);
					return "";
				}
			} else {
				print $langs->trans("Error")." ".$langs->trans("ClassNotFound").' '.$classname;
				return "";
			}
		} else {
			print $langs->trans("ErrorNumberingModuleNotSetup", $this->element);
			return "";
		}
	}

	/**
	 *  Create a document onto disk according to template module.
	 *
	 *  @param	    string		$modele			Force template to use ('' to not force)
	 *  @param		Translate	$outputlangs	object lang a utiliser pour traduction
	 *  @param      int			$hidedetails    Hide details of lines
	 *  @param      int			$hidedesc       Hide description
	 *  @param      int			$hideref        Hide ref
	 *  @param      null|array  $moreparams     Array to provide more information
	 *  @return     int         				0 if KO, 1 if OK
	 */
	public function generateDocument($modele, $outputlangs, $hidedetails = 0, $hidedesc = 0, $hideref = 0, $moreparams = null)
	{
		global $langs;

		$result = 0;
		$includedocgeneration = 1;

		$langs->load("workshop@workshop");

		if (!dol_strlen($modele)) {
			$modele = 'standard_inspectionsheet';

			if (!empty($this->model_pdf)) {
				$modele = $this->model_pdf;
			} elseif (getDolGlobalString('MYOBJECT_ADDON_PDF')) {
				$modele = getDolGlobalString('MYOBJECT_ADDON_PDF');
			}
		}

		$modelpath = "core/modules/workshop/doc/";

		if ($includedocgeneration && !empty($modele)) {
			$result = $this->commonGenerateDocument($modelpath, $modele, $outputlangs, $hidedetails, $hidedesc, $hideref, $moreparams);
		}

		return $result;
	}

	/**
	 * Return validation test result for a field.
	 * Need MAIN_ACTIVATE_VALIDATION_RESULT to be called.
	 *
	 * @param  array   $fields	       		Array of properties of field to show
	 * @param  string  $fieldKey            Key of attribute
	 * @param  string  $fieldValue          value of attribute
	 * @return bool 						Return false if fail, true on success, set $this->error for error message
	 */
	public function validateField($fields, $fieldKey, $fieldValue)
	{
		// Add your own validation rules here.
		// ...

		return parent::validateField($fields, $fieldKey, $fieldValue);
	}

	/**
	 * Action executed by scheduler
	 * CAN BE A CRON TASK. In such a case, parameters come from the schedule job setup field 'Parameters'
	 * Use public function doScheduledJob($param1, $param2, ...) to get parameters
	 *
	 * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
	 */
	public function doScheduledJob()
	{
		//global $conf, $langs;

		//$conf->global->SYSLOG_FILE = 'DOL_DATA_ROOT/dolibarr_mydedicatedlogfile.log';

		$error = 0;
		$this->output = '';
		$this->error = '';

		dol_syslog(__METHOD__." start", LOG_INFO);

		$now = dol_now();

		$this->db->begin();

		// ...

		$this->db->commit();

		dol_syslog(__METHOD__." end", LOG_INFO);

		return $error;
	}
}


require_once DOL_DOCUMENT_ROOT.'/core/class/commonobjectline.class.php';

/**
 * Class InspectionsheetLine. You can also remove this and generate a CRUD class for lines objects.
 */
class InspectionsheetLine extends CommonObjectLine
{
	// To complete with content of an object InspectionsheetLine
	// We should have a field rowid, fk_inspectionsheet and position

	/**
	 * @var int  Does object support extrafields ? 0=No, 1=Yes
	 */
	public $isextrafieldmanaged = 0;

	/**
	 * Constructor
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		$this->db = $db;
	}
}
