<?php
/* Copyright (C) 2024 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    workshop/class/actions_workshop.class.php
 * \ingroup workshop
 * \brief   Example hook overload.
 *
 * Put detailed description here.
 */

require_once DOL_DOCUMENT_ROOT.'/core/class/commonhookactions.class.php';

/**
 * Class ActionsWorkShop
 */
class ActionsWorkShop extends CommonHookActions
{
	/**
	 * @var DoliDB Database handler.
	 */
	public $db;

	/**
	 * @var string Error code (or message)
	 */
	public $error = '';

	/**
	 * @var array Errors
	 */
	public $errors = array();


	/**
	 * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
	 */
	public $results = array();

	/**
	 * @var string String displayed by executeHook() immediately after return
	 */
	public $resprints;

	/**
	 * @var int		Priority of hook (50 is used if value is not defined)
	 */
	public $priority;


	/**
	 * Constructor
	 *
	 *  @param		DoliDB		$db      Database handler
	 */
	public function __construct($db)
	{
		$this->db = $db;
	}


	/**
	 * Execute action
	 *
	 * @param	array			$parameters		Array of parameters
	 * @param	CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param	string			$action      	'add', 'update', 'view'
	 * @return	int         					<0 if KO,
	 *                           				=0 if OK but we want to process standard actions too,
	 *                            				>0 if OK and we want to replace standard actions.
	 */
	public function getNomUrl($parameters, &$object, &$action)
	{
		global $db, $langs, $conf, $user;
		
		
	// 	if ($action == 'confirm_validate' && !GETPOST('cancel', 'alpha')) 
	// 	{

	
       

		
	// 		$url = $_SERVER['REQUEST_URI'];
	// 		$urlQuery = parse_url($url, PHP_URL_QUERY);
	// 		parse_str($urlQuery, $queryParameters);
	// 		$fk_propal = isset($queryParameters['id']) ? $queryParameters['id'] : null;
			
	
	
	
	// 	$objectProject = new Project($db);
	

	// 	$objectpropal = new Propal($db);
	// 		if ($fk_propal ) {
	// 			$ret = $objectpropal->fetch($fk_propal);
	// 			if ($ret > 0) {
	// 				$ret = $objectpropal->fetch_thirdparty();
	
	// 				$socid = $objectpropal->socid;
	
				
	// 			}
	// 			if ($ret <= 0) {
	// 				setEventMessages($objectpropal->error, $objectpropal->errors, 'errors');
	// 				$action = '';
	// 			}
	// 		}else{
	// 			// see out error
	// 		}
		
	
		
	
	// 		$objectInspection = new InspectionSheet($db);
	
	
	
	// 		if ($fk_propal ) {
	// 			$inspectionSheet = $objectInspection->fetchByFkPropal($fk_propal);
			
	// 			$Inspection = $inspectionSheet->Inspection;
	// 			$description = $inspectionSheet->description;
		
	
	// 			if ($inspectionSheet > 0) {
	// 				$inspectionSheet = $objectInspection->fetch_thirdparty();
	
	// 			}
	
	
	// 			if ($inspectionSheet <= 0) {
	// 				setEventMessages($objectInspection->error, $objectInspection->errors, 'errors');
	// 				$action = '';
	// 			}
	// 		}else{
	// 			echo "There is no fk_propal that matchees the record";
	// 		}
	
	// 		// if (!empty($conf->global->PROJECT_CREATE_NO_DRAFT)) {
	// 		// 	$status = Project::STATUS_VALIDATED;
	// 		// }
	
	// 		$defaultref = '';
	// 		$modele = empty($conf->global->PROJECT_ADDON) ? 'mod_project_simple' : $conf->global->PROJECT_ADDON;
	
	// 		// Search template files
	// 		$file = ''; $classname = ''; $filefound = 0;
	// 		$dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);
	// 		foreach ($dirmodels as $reldir) {
	// 			$file = dol_buildpath($reldir."core/modules/project/".$modele.'.php', 0);
	// 			if (file_exists($file)) {
	// 				$filefound = 1;
	// 				$classname = $modele;
	// 				break;
	// 			}
	// 		}
	
	// 		// if ($filefound) {
	// 		// 	$result = dol_include_once($reldir."core/modules/project/".$modele.'.php');
	// 		// 	$modProject = new $classname;
		
	// 		// 	$defaultref = $modProject->getNextValue($thirdparty, $object);
	// 		// }
		
	// 		// if (is_numeric($defaultref) && $defaultref <= 0) {

	// 			if(empty($objectpropal->fk_project)){
	// 			$sql = "SELECT * FROM llx_projet ORDER BY rowid DESC LIMIT 1";			
	// 			$resql = $db->query($sql);

		
	// 			if($resql){
	// 				$obj = $db->fetch_object($resql);		
	// 				$defaultref = generateCustomReferenceP($obj->ref);	
				
	// 				}
			

	// 		$objectProject->ref =  $defaultref;
	// 		$objectProject->title = $Inspection;
	// 		$objectProject->socid = $objectpropal->socid;
	// 		$objectProject->public = 1 ;
	// 		$objectProject->description =$description;
	// 		$objectProject->date_c = dol_now();
	// 		$objectProject->usage_task ? 1 : 0;
	// 		$objectProject->usage_bill_time ? 1 : 0;
	// 		$objectProject->usage_organize_event ? 1 : 0;
	// 		$objectProject->accept_conference_suggestions ? 1 : 0;
	// 		$objectProject->accept_booth_suggestions ? 1 : 0;
	// 		$objectProject->entity = 1;
	// 		$objectProject->statut  = $status;
	// 		$objectProject->fk_propal  = $fk_propal;
	
	// 		// // Fill array 'array_options' with data from add form
	// 		// $ret = $extrafields->setOptionalsFromPost(null, $object);
	// 		// if ($ret < 0) {
	// 		// 	$error++;
	// 		// }
	
	// 		$result = $objectProject->create($user);
	

	// 		if($result){
	
	
	// 			$ret = $objectpropal->fetch($fk_propal);
	// 			if($ret){
	
	// 			$ret = $objectpropal->fetch_thirdparty();
					
	
	// 			$rowid = $objectpropal->id;
	// 			$fk_projet =  $result;
				
	// 			updatePropal($db,$fk_projet,$rowid);
	// 			}
	
	// 		}
	
	
	// 		if (!$error && $result > 0) {
	// 			$typeofcontact = 'PROJECTLEADER';
	// 			$result = $objectProject->add_contact($user->id, $typeofcontact, 'internal');
	
	
	// 			if ($result == -3) {
	// 				setEventMessage('ErrorPROJECTLEADERRoleMissingRestoreIt', 'errors');
	// 				$error++;
	// 			} elseif ($result < 0) {
	// 				$langs->load("errors");
	// 				setEventMessages($objectProject->error, $objectProject->errors, 'errors');
	// 				$error++;
	// 			}
	
	// 		} else {
	// 			$langs->load("errors");
	// 			setEventMessages($objectProject->error, $objectProject->errors, 'errors');
	// 			$error++;
	// 		}
	// 		if (!$error && !empty($objectProject->id) > 0) {
	// 			// Category association
	// 			$categories = 5 ;
	// 			$result = $objectProject->setCategories($categories);
	// 			if ($result < 0) {
	// 				$langs->load("errors");
	// 				setEventMessages($objectProject->error, $objectProject->errors, 'errors');
	// 				$error++;
	// 			}
	// 		}
	
		
	
	// 			$objectTask = new Task($db);

				
	// 			$results = getInspectionWorkOrderItems($db,$fk_propal);
	
	
	
	// 			$fk_project = getProjectId($db,$fk_propal);
	
	// 			foreach ($results as $data) {
					
	
	
	// 				$Date = date('Y-m-d H:i:s');
				
	// 				$currentDate = $db->escape($Date);
	
	// 					$sql = "SELECT * FROM llx_projet_task ORDER BY rowid DESC LIMIT 1";		
						
						
	// 					$resql = $db->query($sql);
	
	// 					if($resql){
	
	// 					$obj = $db->fetch_object($resql);		
					
							
	// 					if($obj){
	
	// 							$defaultref  = generateCustomReferenceT($obj->ref);	
	// 							}else{
	// 								$defaultref  = generateCustomReferenceT($obj->ref);	
	
	// 							}

					
	// 					}else
	// 					{
	// 						print $db->error();
	// 					}
						
				
	// 					$objectTask->ref  =$defaultref;
	// 					$objectTask->entity =1;
	// 					$objectTask->fk_task_parent = $fk_project;
	// 					$objectTask->label =  $db->escape($data['workorder_desc']);
	// 					$objectTask->description = $db->escape($data['label']);
	// 					$objectTask->planned_workload = 5;
	// 					$objectTask->date_c = $currentDate;
	// 					$objectTask->duration_effective = 5;
	// 					$objectTask->priority = 1;
	// 					$objectTask->fk_project = $fk_project;
	

	// 					$taskid = $objectTask->create($user);
					
	// 					if ($taskid > 0) {
	// 						$result = $objectTask->add_contact($user->id, 'TASKEXECUTIVE', 'internal');
	// 					}
	// 					 else {
	// 						if ($db->lasterrno() == 'DB_ERROR_RECORD_ALREADY_EXISTS') {
	// 							$langs->load("projects");
	// 							setEventMessages($langs->trans('NewTaskRefSuggested'), null, 'warnings');
	// 							$duplicate_code_error = true;
	// 						} else {
	// 							setEventMessages($objectTask->error, $task->errors, 'errors');
	// 						}
	// 						$action = 'create';
	// 						$error++;
	// 					}
	// 				}
	// 			}else{
	// 			}

	// }else
	if($action == 'editline'){

		$url = $_SERVER['REQUEST_URI'];
		$urlQuery = parse_url($url, PHP_URL_QUERY);
		parse_str($urlQuery, $queryParameters);
		$fk_propal = isset($queryParameters['id']) ? $queryParameters['id'] : null;
		$lineid = isset($queryParameters['lineid']) ? $queryParameters['lineid'] : null;

		

		
		if ($fk_propal !== null) {
			$base_path = DOL_DOCUMENT_ROOT;
			$file_path = '/dolibarr-develop/htdocs/custom/workshop/inspectionsheet_card.php';
			$csrfToken = newToken();
			$form_action_url = $file_path . '?lineid= '.urlencode($lineid ) .'&action=edit' . '&token='. $csrfToken;
			
			// Generate a CSRF token (replace 'your_csrf_token_here' with your actual token)
			

			// Add the CSRF token as an HTTP header
			$headers = array(
				'X-CSRF-Token: ' . $csrfToken
			);

			// Send the request with the CSRF token in headers
			$ch = curl_init($form_action_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$response = curl_exec($ch);
			curl_close($ch);



			print '<script>window.location.href = "' . $form_action_url . '";</script>';

		}
		
	}elseif($action == 'modif'){

			

		$url = $_SERVER['REQUEST_URI'];
		$urlQuery = parse_url($url, PHP_URL_QUERY);
		parse_str($urlQuery, $queryParameters);
		$fk_propal = isset($queryParameters['id']) ? $queryParameters['id'] : null;
		


		$objectpropal = new Propal($db);
			if ($fk_propal ) {
				$ret = $objectpropal->fetch($fk_propal);
				if ($ret > 0) {
					
					$objectProject = new Project($db);
					$results = $objectProject->fetch($objectpropal->fk_project);

					$objectProject->id = $objectpropal->fk_project;
					$objectProject->delete($user);
					

	
				
				}
			
			}

			
	

		}
		$this->resprints = '';
		return 0;
}

	/**
	 * Overloading the doActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             Return integer < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {	    // do something only for the context 'somecontext1' or 'somecontext2'
			// Do what you want here...
			// You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the doMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             Return integer < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {		// do something only for the context 'somecontext1' or 'somecontext2'
			foreach ($parameters['toselect'] as $objectid) {
				// Do action on each object id
			}
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the addMoreMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             Return integer < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function addMoreMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter
		$disabled = 1;

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {		// do something only for the context 'somecontext1' or 'somecontext2'
			$this->resprints = '<option value="0"'.($disabled ? ' disabled="disabled"' : '').'>'.$langs->trans("WorkShopMassAction").'</option>';
		}

		if (!$error) {
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}



	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$object		   	Object output on PDF
	 * @param   string	$action     	'add', 'update', 'view'
	 * @return  int 		        	Return integer <0 if KO,
	 *                          		=0 if OK but we want to process standard actions too,
	 *  	                            >0 if OK and we want to replace standard actions.
	 */
	public function beforePDFCreation($parameters, &$object, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0;
		$deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {		// do something only for the context 'somecontext1' or 'somecontext2'
		}

		return $ret;
	}

	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$pdfhandler     PDF builder handler
	 * @param   string	$action         'add', 'update', 'view'
	 * @return  int 		            Return integer <0 if KO,
	 *                                  =0 if OK but we want to process standard actions too,
	 *                                  >0 if OK and we want to replace standard actions.
	 */
	public function afterPDFCreation($parameters, &$pdfhandler, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0;
		$deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {
			// do something only for the context 'somecontext1' or 'somecontext2'
		}

		return $ret;
	}



	/**
	 * Overloading the loadDataForCustomReports function : returns data to complete the customreport tool
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             Return integer < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function loadDataForCustomReports($parameters, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$langs->load("workshop@workshop");

		$this->results = array();

		$head = array();
		$h = 0;

		if ($parameters['tabfamily'] == 'workshop') {
			$head[$h][0] = dol_buildpath('/module/index.php', 1);
			$head[$h][1] = $langs->trans("Home");
			$head[$h][2] = 'home';
			$h++;

			$this->results['title'] = $langs->trans("WorkShop");
			$this->results['picto'] = 'workshop@workshop';
		}

		$head[$h][0] = 'customreports.php?objecttype='.$parameters['objecttype'].(empty($parameters['tabfamily']) ? '' : '&tabfamily='.$parameters['tabfamily']);
		$head[$h][1] = $langs->trans("CustomReports");
		$head[$h][2] = 'customreports';

		$this->results['head'] = $head;

		return 1;
	}



	/**
	 * Overloading the restrictedArea function : check permission on an object
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int 		      			  	Return integer <0 if KO,
	 *                          				=0 if OK but we want to process standard actions too,
	 *  	                            		>0 if OK and we want to replace standard actions.
	 */
	public function restrictedArea($parameters, &$action, $hookmanager)
	{
		global $user;

		if ($parameters['features'] == 'myobject') {
			if ($user->hasRight('workshop', 'myobject', 'read')) {
				$this->results['result'] = 1;
				return 1;
			} else {
				$this->results['result'] = 0;
				return 1;
			}
		}

		return 0;
	}

	/**
	 * Execute action completeTabsHead
	 *
	 * @param   array           $parameters     Array of parameters
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         'add', 'update', 'view'
	 * @param   Hookmanager     $hookmanager    hookmanager
	 * @return  int                             Return integer <0 if KO,
	 *                                          =0 if OK but we want to process standard actions too,
	 *                                          >0 if OK and we want to replace standard actions.
	 */
	public function completeTabsHead(&$parameters, &$object, &$action, $hookmanager)
	{
		global $langs, $conf, $user;

		if (!isset($parameters['object']->element)) {
			return 0;
		}
		if ($parameters['mode'] == 'remove') {
			// used to make some tabs removed
			return 0;
		} elseif ($parameters['mode'] == 'add') {
			$langs->load('workshop@workshop');
			// used when we want to add some tabs
			$counter = count($parameters['head']);
			$element = $parameters['object']->element;
			$id = $parameters['object']->id;
			// verifier le type d'onglet comme member_stats où ça ne doit pas apparaitre
			// if (in_array($element, ['societe', 'member', 'contrat', 'fichinter', 'project', 'propal', 'commande', 'facture', 'order_supplier', 'invoice_supplier'])) {
			if (in_array($element, ['context1', 'context2'])) {
				$datacount = 0;

				$parameters['head'][$counter][0] = dol_buildpath('/workshop/workshop_tab.php', 1) . '?id=' . $id . '&amp;module='.$element;
				$parameters['head'][$counter][1] = $langs->trans('WorkShopTab');
				if ($datacount > 0) {
					$parameters['head'][$counter][1] .= '<span class="badge marginleftonlyshort">' . $datacount . '</span>';
				}
				$parameters['head'][$counter][2] = 'workshopemails';
				$counter++;
			}
			if ($counter > 0 && (int) DOL_VERSION < 14) {
				$this->results = $parameters['head'];
				// return 1 to replace standard code
				return 1;
			} else {
				// en V14 et + $parameters['head'] est modifiable par référence
				return 0;
			}
		} else {
			// Bad value for $parameters['mode']
			return -1;
		}
	}

	/* Add here any other hooked methods... */
}
