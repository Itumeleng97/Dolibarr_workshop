<?php
/* Copyright (C) 2004-2018  Laurent Destailleur     <eldy@users.sourceforge.net>
 * Copyright (C) 2018-2019  Nicolas ZABOURI         <info@inovea-conseil.com>
 * Copyright (C) 2019-2020  Frédéric France         <frederic.france@netlogic.fr>
 * Copyright (C) 2024 SuperAdmin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   workshop     Module WorkShop
 *  \brief      WorkShop module descriptor.
 *
 *  \file       htdocs/workshop/core/modules/modWorkShop.class.php
 *  \ingroup    workshop
 *  \brief      Description and activation file for module WorkShop
 */
include_once DOL_DOCUMENT_ROOT.'/core/modules/DolibarrModules.class.php';

/**
 *  Description and activation class for module WorkShop
 */
class modWorkShop extends DolibarrModules
{
	/**
	 * Constructor. Define names, constants, directories, boxes, permissions
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct($db)
	{
		global $langs, $conf;
		$this->db = $db;

		// Id for module (must be unique).
		// Use here a free id (See in Home -> System information -> Dolibarr for list of used modules id).
		$this->numero = 500976; // TODO Go on page https://wiki.dolibarr.org/index.php/List_of_modules_id to reserve an id number for your module

		// Key text used to identify module (for permissions, menus, etc...)
		$this->rights_class = 'workshop';

		// Family can be 'base' (core modules),'crm','financial','hr','projects','products','ecm','technic' (transverse modules),'interface' (link with external tools),'other','...'
		// It is used to group modules by family in module setup page
		$this->family = "other";

		// Module position in the family on 2 digits ('01', '10', '20', ...)
		$this->module_position = '90';

		// Gives the possibility for the module, to provide his own family info and position of this family (Overwrite $this->family and $this->module_position. Avoid this)
		//$this->familyinfo = array('myownfamily' => array('position' => '01', 'label' => $langs->trans("MyOwnFamily")));
		// Module label (no space allowed), used if translation string 'ModuleWorkShopName' not found (WorkShop is name of module).
		$this->name = preg_replace('/^mod/i', '', get_class($this));

		// Module description, used if translation string 'ModuleWorkShopDesc' not found (WorkShop is name of module).
		$this->description = "WorkShopDescription";
		// Used only if file README.md and README-LL.md not found.
		$this->descriptionlong = "WorkShopDescription";

		// Author
		$this->editor_name = 'Mohlala';
		$this->editor_url = '';

		// Possible values for version are: 'development', 'experimental', 'dolibarr', 'dolibarr_deprecated', 'experimental_deprecated' or a version string like 'x.y.z'
		$this->version = '1.0';
		// Url to the file with your last numberversion of this module
		//$this->url_last_version = 'http://www.example.com/versionmodule.txt';

		// Key used in llx_const table to save module status enabled/disabled (where WORKSHOP is value of property name of module in uppercase)
		$this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);

		// Name of image file used for this module.
		// If file is in theme/yourtheme/img directory under name object_pictovalue.png, use this->picto='pictovalue'
		// If file is in module/img directory under name object_pictovalue.png, use this->picto='pictovalue@module'
		// To use a supported fa-xxx css style of font awesome, use this->picto='xxx'
		$this->picto = 'fa-file-o';

		// Define some features supported by module (triggers, login, substitutions, menus, css, etc...)
		$this->module_parts = array(
			// Set this to 1 if module has its own trigger directory (core/triggers)
			'triggers' => 0,
			// Set this to 1 if module has its own login method file (core/login)
			'login' => 0,
			// Set this to 1 if module has its own substitution function file (core/substitutions)
			'substitutions' => 0,
			// Set this to 1 if module has its own menus handler directory (core/menus)
			'menus' => 0,
			// Set this to 1 if module overwrite template dir (core/tpl)
			'tpl' => 0,
			// Set this to 1 if module has its own barcode directory (core/modules/barcode)
			'barcode' => 0,
			// Set this to 1 if module has its own models directory (core/modules/xxx)
			'models' => 1,
			// Set this to 1 if module has its own printing directory (core/modules/printing)
			'printing' => 0,
			// Set this to 1 if module has its own theme directory (theme)
			'theme' => 0,
			// Set this to relative path of css file if module has its own css file
			'css' => array(
				//    '/workshop/css/workshop.css.php',
			),
			// Set this to relative path of js file if module must load a js on all pages
			'js' => array(
				//   '/workshop/js/workshop.js.php',
			),
			// Set here all hooks context managed by module. To find available hook context, make a "grep -r '>initHooks(' *" on source code. You can also set hook context to 'all'
			'hooks' => array(
				  'data' => array(
				      'propalcard',
				      'globalcard',
				  ),
				  'entity' => '0',
			),
			// Set this to 1 if features of module are opened to external users
			'moduleforexternal' => 0,
			// Set this to 1 if the module provides a website template into doctemplates/websites/website_template-mytemplate
			'websitetemplates' => 0
		);

		// Data directories to create when module is enabled.
		// Example: this->dirs = array("/workshop/temp","/workshop/subdir");
		$this->dirs = array("/workshop/temp");

		// Config pages. Put here list of php page, stored into workshop/admin directory, to use to setup module.
		$this->config_page_url = array("setup.php@workshop");

		// Dependencies
		// A condition to hide module
		$this->hidden = false;
		// List of module class names that must be enabled if this module is enabled. Example: array('always'=>array('modModuleToEnable1','modModuleToEnable2'), 'FR'=>array('modModuleToEnableFR')...)
		$this->depends = array();
		// List of module class names to disable if this one is disabled. Example: array('modModuleToDisable1', ...)
		$this->requiredby = array();
		// List of module class names this module is in conflict with. Example: array('modModuleToDisable1', ...)
		$this->conflictwith = array();

		// The language file dedicated to your module
		$this->langfiles = array("workshop@workshop");

		// Prerequisites
		$this->phpmin = array(7, 0); // Minimum version of PHP required by module
		$this->need_dolibarr_version = array(11, -3); // Minimum version of Dolibarr required by module
		$this->need_javascript_ajax = 0;

		// Messages at activation
		$this->warnings_activation = array(); // Warning to show when we activate module. array('always'='text') or array('FR'='textfr','MX'='textmx'...)
		$this->warnings_activation_ext = array(); // Warning to show when we activate an external module. array('always'='text') or array('FR'='textfr','MX'='textmx'...)
		//$this->automatic_activation = array('FR'=>'WorkShopWasAutomaticallyActivatedBecauseOfYourCountryChoice');
		//$this->always_enabled = true;								// If true, can't be disabled

		// Constants
		// List of particular constants to add when module is enabled (key, 'chaine', value, desc, visible, 'current' or 'allentities', deleteonunactive)
		// Example: $this->const=array(1 => array('WORKSHOP_MYNEWCONST1', 'chaine', 'myvalue', 'This is a constant to add', 1),
		//                             2 => array('WORKSHOP_MYNEWCONST2', 'chaine', 'myvalue', 'This is another constant to add', 0, 'current', 1)
		// );
		$this->const = array();

		// Some keys to add into the overwriting translation tables
		/*$this->overwrite_translation = array(
			'en_US:ParentCompany'=>'Parent company or reseller',
			'fr_FR:ParentCompany'=>'Maison mère ou revendeur'
		)*/

		if (!isModEnabled("workshop")) {
			$conf->workshop = new stdClass();
			$conf->workshop->enabled = 0;
		}

		// Array to add new pages in new tabs
		$this->tabs = array();
		// Example:
		// $this->tabs[] = array('data'=>'objecttype:+tabname1:Title1:mylangfile@workshop:$user->hasRight('workshop', 'read'):/workshop/mynewtab1.php?id=__ID__');  					// To add a new tab identified by code tabname1
		$this->tabs[] = array('data'=>'propal:+Inspection:InspectionSheet:propal@1::/custom/workshop/inspectionsheet_card.php?id=__ID__'); 	// To add another new tab identified by code tabname2. Label will be result of calling all substitution functions on 'Title2' key.
		// $this->tabs[] = array('data'=>'objecttype:-tabname:NU:conditiontoremove');                                                     										// To remove an existing tab identified by code tabname
		//
		// Where objecttype can be
		// 'categories_x'	  to add a tab in category view (replace 'x' by type of category (0=product, 1=supplier, 2=customer, 3=member)
		// 'contact'          to add a tab in contact view
		// 'contract'         to add a tab in contract view
		// 'group'            to add a tab in group view
		// 'intervention'     to add a tab in intervention view
		// 'invoice'          to add a tab in customer invoice view
		// 'invoice_supplier' to add a tab in supplier invoice view
		// 'member'           to add a tab in foundation member view
		// 'opensurveypoll'	  to add a tab in opensurvey poll view
		// 'order'            to add a tab in sale order view
		// 'order_supplier'   to add a tab in supplier order view
		// 'payment'		  to add a tab in payment view
		// 'payment_supplier' to add a tab in supplier payment view
		// 'product'          to add a tab in product view
		// 'propal'           to add a tab in propal view
		// 'project'          to add a tab in project view
		// 'stock'            to add a tab in stock view
		// 'thirdparty'       to add a tab in third party view
		// 'user'             to add a tab in user view

		// Dictionaries
		/* Example:
		 $this->dictionaries=array(
		 'langs'=>'workshop@workshop',
		 // List of tables we want to see into dictonnary editor
		 'tabname'=>array("table1", "table2", "table3"),
		 // Label of tables
		 'tablib'=>array("Table1", "Table2", "Table3"),
		 // Request to select fields
		 'tabsql'=>array('SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table1 as f', 'SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table2 as f', 'SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table3 as f'),
		 // Sort order
		 'tabsqlsort'=>array("label ASC", "label ASC", "label ASC"),
		 // List of fields (result of select to show dictionary)
		 'tabfield'=>array("code,label", "code,label", "code,label"),
		 // List of fields (list of fields to edit a record)
		 'tabfieldvalue'=>array("code,label", "code,label", "code,label"),
		 // List of fields (list of fields for insert)
		 'tabfieldinsert'=>array("code,label", "code,label", "code,label"),
		 // Name of columns with primary key (try to always name it 'rowid')
		 'tabrowid'=>array("rowid", "rowid", "rowid"),
		 // Condition to show each dictionary
		 'tabcond'=>array(isModEnabled('workshop'), isModEnabled('workshop'), isModEnabled('workshop')),
		 // Tooltip for every fields of dictionaries: DO NOT PUT AN EMPTY ARRAY
		 'tabhelp'=>array(array('code'=>$langs->trans('CodeTooltipHelp'), 'field2' => 'field2tooltip'), array('code'=>$langs->trans('CodeTooltipHelp'), 'field2' => 'field2tooltip'), ...),
		 );
		 */
		/* BEGIN MODULEBUILDER DICTIONARIES */
		$this->dictionaries = array();
		/* END MODULEBUILDER DICTIONARIES */

		// Boxes/Widgets
		// Add here list of php file(s) stored in workshop/core/boxes that contains a class to show a widget.
		/* BEGIN MODULEBUILDER WIDGETS */
		$this->boxes = array(
			 0 => array(
			     'file' => 'workshopwidget1.php@workshop',
			     'note' => 'Widget provided by WorkShop',
			     'enabledbydefaulton' => 'Home',
			 ),
			
		);
		/* END MODULEBUILDER WIDGETS */

		// Cronjobs (List of cron jobs entries to add when module is enabled)
		// unit_frequency must be 60 for minute, 3600 for hour, 86400 for day, 604800 for week
		/* BEGIN MODULEBUILDER CRON */
		$this->cronjobs = array(
			//  0 => array(
			//      'label' => 'MyJob label',
			//      'jobtype' => 'method',
			//      'class' => '/workshop/class/inspectiontype.class.php',
			//      'objectname' => 'InspectionType',
			//      'method' => 'doScheduledJob',
			//      'parameters' => '',
			//      'comment' => 'Comment',
			//      'frequency' => 2,
			//      'unitfrequency' => 3600,
			//      'status' => 0,
			//      'test' => 'isModEnabled("workshop")',
			//      'priority' => 50,
			//  ),
		);
		/* END MODULEBUILDER CRON */
		// Example: $this->cronjobs=array(
		//    0=>array('label'=>'My label', 'jobtype'=>'method', 'class'=>'/dir/class/file.class.php', 'objectname'=>'MyClass', 'method'=>'myMethod', 'parameters'=>'param1, param2', 'comment'=>'Comment', 'frequency'=>2, 'unitfrequency'=>3600, 'status'=>0, 'test'=>'isModEnabled("workshop")', 'priority'=>50),
		//    1=>array('label'=>'My label', 'jobtype'=>'command', 'command'=>'', 'parameters'=>'param1, param2', 'comment'=>'Comment', 'frequency'=>1, 'unitfrequency'=>3600*24, 'status'=>0, 'test'=>'isModEnabled("workshop")', 'priority'=>50)
		// );

		// Permissions provided by this module
		$this->rights = array();
		$r = 0;
		// Add here entries to declare new permissions
		/* BEGIN MODULEBUILDER PERMISSIONS */
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (0 * 10) + 0 + 1);
		$this->rights[$r][1] = 'Read InspectionType object of WorkShop';
		$this->rights[$r][4] = 'inspectiontype';
		$this->rights[$r][5] = 'read';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (0 * 10) + 1 + 1);
		$this->rights[$r][1] = 'Create/Update InspectionType object of WorkShop';
		$this->rights[$r][4] = 'inspectiontype';
		$this->rights[$r][5] = 'write';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (0 * 10) + 2 + 1);
		$this->rights[$r][1] = 'Delete InspectionType object of WorkShop';
		$this->rights[$r][4] = 'inspectiontype';
		$this->rights[$r][5] = 'delete';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (1 * 10) + 0 + 1);
		$this->rights[$r][1] = 'Read InspectionHeader object of WorkShop';
		$this->rights[$r][4] = 'inspectionheader';
		$this->rights[$r][5] = 'read';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (1 * 10) + 1 + 1);
		$this->rights[$r][1] = 'Create/Update InspectionHeader object of WorkShop';
		$this->rights[$r][4] = 'inspectionheader';
		$this->rights[$r][5] = 'write';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (1 * 10) + 2 + 1);
		$this->rights[$r][1] = 'Delete InspectionHeader object of WorkShop';
		$this->rights[$r][4] = 'inspectionheader';
		$this->rights[$r][5] = 'delete';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (2 * 10) + 0 + 1);
		$this->rights[$r][1] = 'Read CausesOfFailure object of WorkShop';
		$this->rights[$r][4] = 'causesoffailure';
		$this->rights[$r][5] = 'read';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (2 * 10) + 1 + 1);
		$this->rights[$r][1] = 'Create/Update CausesOfFailure object of WorkShop';
		$this->rights[$r][4] = 'causesoffailure';
		$this->rights[$r][5] = 'write';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (2 * 10) + 2 + 1);
		$this->rights[$r][1] = 'Delete CausesOfFailure object of WorkShop';
		$this->rights[$r][4] = 'causesoffailure';
		$this->rights[$r][5] = 'delete';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (3 * 10) + 0 + 1);
		$this->rights[$r][1] = 'Read Inspectionsheet object of WorkShop';
		$this->rights[$r][4] = 'inspectionsheet';
		$this->rights[$r][5] = 'read';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (3 * 10) + 1 + 1);
		$this->rights[$r][1] = 'Create/Update Inspectionsheet object of WorkShop';
		$this->rights[$r][4] = 'inspectionsheet';
		$this->rights[$r][5] = 'write';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf('%02d', (3 * 10) + 2 + 1);
		$this->rights[$r][1] = 'Delete Inspectionsheet object of WorkShop';
		$this->rights[$r][4] = 'inspectionsheet';
		$this->rights[$r][5] = 'delete';
		$r++;
		
		/* END MODULEBUILDER PERMISSIONS */

		// Main menu entries to add
		$this->menu = array();
		$r = 0;
		// Add here entries to declare new menus
		/* BEGIN MODULEBUILDER TOPMENU */
		$this->menu[$r++] = array(
			'fk_menu'=>'fk_mainmenu=workshop',  // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'top', // This is a Top menu entry
			'titre'=>'ModuleWorkShopName',
			'prefix' => img_picto('', $this->picto, 'class="pictofixedwidth valignmiddle"'),
			'mainmenu'=>'workshop',
			'leftmenu'=>'',
			'url'=>'/workshop/workshopindex.php',
			'langs'=>'workshop@workshop', // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000 + $r,
			'enabled'=>'isModEnabled("workshop")', // Define condition to show or hide menu entry. Use 'isModEnabled("workshop")' if entry must be visible if module is enabled.
			'perms'=>'1', // Use 'perms'=>'$user->hasRight("workshop", "inspectiontype", "read")' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2, // 0=Menu for internal users, 1=external users, 2=both
		);
		/* END MODULEBUILDER TOPMENU */
		/* BEGIN MODULEBUILDER LEFTMENU MYOBJECT */
		/* LEFTMENU NEW_INSPECTIONTYPE */

		$this->menu[$r++]=array(
			'fk_menu' => 'fk_mainmenu=workshop',
			'type' => 'left',
			'titre' => 'Inspectiontype',
			'mainmenu' => 'workshop',
			'leftmenu' => 'inspectiontype',
			'prefix' => img_picto('', $this->picto, 'class="paddingright pictofixedwidth valignmiddle"'),
		   //  'url' => '/workshop/inspectionsheet_card.php',
			'langs' => 'workshop@workshop',
			'position' => 1000,
			'enabled' => 'isModEnabled(\'workshop\')',
			'perms' => '$user->hasRight(\'workshop\', \'inspectiontype\', \'read\')',
			'target' => '',
			'user' => 2,
	   );
	   /* END LEFTMENU INSPECTIONSHEET */
	   /* LEFTMENU LIST INSPECTIONSHEET */
	   $this->menu[$r++]=array(
			'fk_menu' => 'fk_mainmenu=workshop,fk_leftmenu=inspectiontype',
			'type' => 'left',
			'titre' => 'New Inspectiontype',
			'mainmenu' => 'workshop',
			'leftmenu' => 'workshop_inspectiontype_new',
			'url' => '/workshop/inspectiontype_card.php?action=create',
			'langs' => 'workshop@workshop',
			'position' => 1000,
			'enabled' => 'isModEnabled(\'workshop\')',
			'perms' => '$user->hasRight(\'workshop\', \'inspectiontype\', \'read\')',
			'target' => '',
			'user' => 2,
	   );
	   /* END LEFTMENU LIST INSPECTIONSHEET */
	   /* LEFTMENU NEW INSPECTIONSHEET */
	   $this->menu[$r++]=array(
			'fk_menu' => 'fk_mainmenu=workshop,fk_leftmenu=inspectiontype',
			'type' => 'left',
			'titre' => 'Inspectiontype List',
			'mainmenu' => 'workshop',
			'leftmenu' => 'workshop_inspectiontype_list',
			'url' => '/workshop/inspectiontype_list.php',
			'langs' => 'workshop@workshop',
			'position' => 1000,
			'enabled' => 'isModEnabled(\'workshop\')',
			'perms' => '$user->hasRight(\'workshop\', \'inspectiontype\', \'write\')',
			'target' => '',
			'user' => 2,
	   );
		$this->menu[$r++]=array(
			 'fk_menu' => 'fk_mainmenu=workshop',
			 'type' => 'left',
			 'titre' => 'Inspectionheader',
			 'mainmenu' => 'workshop',
			 'leftmenu' => 'inspectionheader',
			 'prefix' => img_picto('', $this->picto, 'class="paddingright pictofixedwidth valignmiddle"'),
			//  'url' => '/workshop/inspectiontype_card.php?action=create',
			 'langs' => 'workshop@workshop',
			 'position' => 10001,
			 'enabled' => 'isModEnabled(\'workshop\')',
			 'perms' => '$user->hasRight(\'workshop\', \'inspectiontype\', \'write\')',
			 'target' => '',
			 'user' => 2,
		);
		/* END LEFTMENU NEW_INSPECTIONTYPE */
		/* LEFTMENU LIST_INSPECTIONTYPE */
		$this->menu[$r++]=array(
			 'fk_menu' => 'fk_mainmenu=workshop,fk_leftmenu=inspectionheader',
			 'type' => 'left',
			 'titre' => 'New Inspectionheader',
			 'mainmenu' => 'workshop',
			 'leftmenu' => 'workshop_inspectionheader_card',
			 'url' => '/workshop/inspectionheader_card.php?action=create',
			 'langs' => 'workshop@workshop',
			 'position' => 10001,
			 'enabled' => 'isModEnabled(\'workshop\')',
			 'perms' => '$user->hasRight(\'workshop\', \'inspectionheader\', \'read\')',
			 'target' => '',
			 'user' => 2,
		);
		$this->menu[$r++]=array(
			'fk_menu' => 'fk_mainmenu=workshop,fk_leftmenu=inspectionheader',
			'type' => 'left',
			'titre' => 'Inspectionheader List',
			'mainmenu' => 'workshop',
			'leftmenu' => 'workshop_inspectionheader_list',
			'url' => '/workshop/inspectionheader_list.php',
			'langs' => 'workshop@workshop',
			'position' => 10001,
			'enabled' => 'isModEnabled(\'workshop\')',
			'perms' => '$user->hasRight(\'workshop\', \'inspectionheader\', \'read\')',
			'target' => '',
			'user' => 2,
	   );


	   $this->menu[$r++]=array(
		'fk_menu' => 'fk_mainmenu=workshop',
		'type' => 'left',
		'titre' => 'Inspection CausesOfFailure',
		'mainmenu' => 'workshop',
		'leftmenu' => 'causesoffailure',
		'prefix' => img_picto('', $this->picto, 'class="paddingright pictofixedwidth valignmiddle"'),
	   //  'url' => '/workshop/inspectiontype_card.php?action=create',
		'langs' => 'workshop@workshop',
		'position' => 10002,
		'enabled' => 'isModEnabled(\'workshop\')',
		'perms' => '$user->hasRight(\'workshop\', \'causesoffailure\', \'write\')',
		'target' => '',
		'user' => 2,
   );
   
   $this->menu[$r++]=array(
		'fk_menu' => 'fk_mainmenu=workshop,fk_leftmenu=causesoffailure',
		'type' => 'left',
		'titre' => 'New CausesOfFailure',
		'mainmenu' => 'workshop',
		'leftmenu' => 'workshop_causesoffailure_card',
		'url' => '/workshop/causesoffailure_card.php?action=create',
		'langs' => 'workshop@workshop',
		'position' => 10002,
		'enabled' => 'isModEnabled(\'workshop\')',
		'perms' => '$user->hasRight(\'workshop\', \'causesoffailure\', \'read\')',
		'target' => '',
		'user' => 2,
   );
   $this->menu[$r++]=array(
	   'fk_menu' => 'fk_mainmenu=workshop,fk_leftmenu=causesoffailure',
	   'type' => 'left',
	   'titre' => 'Causesoffailure List',
	   'mainmenu' => 'workshop',
	   'leftmenu' => 'workshop_causesoffailure_list',
	   'url' => '/workshop/causesoffailure_list.php',
	   'langs' => 'workshop@workshop',
	   'position' => 10002,
	   'enabled' => 'isModEnabled(\'workshop\')',
	   'perms' => '$user->hasRight(\'workshop\', \'causesoffailure\', \'read\')',
	   'target' => '',
	   'user' => 2,
  );
		/* END LEFTMENU LIST_INSPECTIONTYPE */
		/* LEFTMENU INSPECTIONSHEET */

		/* END LEFTMENU NEW INSPECTIONSHEET */


		/* END MODULEBUILDER LEFTMENU MYOBJECT */
		// Exports profiles provided by this module
		$r = 1;
		/* BEGIN MODULEBUILDER EXPORT MYOBJECT */
		/*
		$langs->load("workshop@workshop");
		$this->export_code[$r]=$this->rights_class.'_'.$r;
		$this->export_label[$r]='InspectionTypeLines';	// Translation key (used only if key ExportDataset_xxx_z not found)
		$this->export_icon[$r]='inspectiontype@workshop';
		// Define $this->export_fields_array, $this->export_TypeFields_array and $this->export_entities_array
		$keyforclass = 'InspectionType'; $keyforclassfile='/workshop/class/inspectiontype.class.php'; $keyforelement='inspectiontype@workshop';
		include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		//$this->export_fields_array[$r]['t.fieldtoadd']='FieldToAdd'; $this->export_TypeFields_array[$r]['t.fieldtoadd']='Text';
		//unset($this->export_fields_array[$r]['t.fieldtoremove']);
		//$keyforclass = 'InspectionTypeLine'; $keyforclassfile='/workshop/class/inspectiontype.class.php'; $keyforelement='inspectiontypeline@workshop'; $keyforalias='tl';
		//include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		$keyforselect='inspectiontype'; $keyforaliasextra='extra'; $keyforelement='inspectiontype@workshop';
		include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		//$keyforselect='inspectiontypeline'; $keyforaliasextra='extraline'; $keyforelement='inspectiontypeline@workshop';
		//include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		//$this->export_dependencies_array[$r] = array('inspectiontypeline'=>array('tl.rowid','tl.ref')); // To force to activate one or several fields if we select some fields that need same (like to select a unique key if we ask a field of a child to avoid the DISTINCT to discard them, or for computed field than need several other fields)
		//$this->export_special_array[$r] = array('t.field'=>'...');
		//$this->export_examplevalues_array[$r] = array('t.field'=>'Example');
		//$this->export_help_array[$r] = array('t.field'=>'FieldDescHelp');
		$this->export_sql_start[$r]='SELECT DISTINCT ';
		$this->export_sql_end[$r]  =' FROM '.MAIN_DB_PREFIX.'inspectiontype as t';
		//$this->export_sql_end[$r]  =' LEFT JOIN '.MAIN_DB_PREFIX.'inspectiontype_line as tl ON tl.fk_inspectiontype = t.rowid';
		$this->export_sql_end[$r] .=' WHERE 1 = 1';
		$this->export_sql_end[$r] .=' AND t.entity IN ('.getEntity('inspectiontype').')';
		$r++; */
		/* END MODULEBUILDER EXPORT MYOBJECT */

		// Imports profiles provided by this module
		$r = 1;
		/* BEGIN MODULEBUILDER IMPORT MYOBJECT */
		/*
		$langs->load("workshop@workshop");
		$this->import_code[$r]=$this->rights_class.'_'.$r;
		$this->import_label[$r]='InspectionTypeLines';	// Translation key (used only if key ExportDataset_xxx_z not found)
		$this->import_icon[$r]='inspectiontype@workshop';
		$this->import_tables_array[$r] = array('t' => MAIN_DB_PREFIX.'workshop_inspectiontype', 'extra' => MAIN_DB_PREFIX.'workshop_inspectiontype_extrafields');
		$this->import_tables_creator_array[$r] = array('t' => 'fk_user_author'); // Fields to store import user id
		$import_sample = array();
		$keyforclass = 'InspectionType'; $keyforclassfile='/workshop/class/inspectiontype.class.php'; $keyforelement='inspectiontype@workshop';
		include DOL_DOCUMENT_ROOT.'/core/commonfieldsinimport.inc.php';
		$import_extrafield_sample = array();
		$keyforselect='inspectiontype'; $keyforaliasextra='extra'; $keyforelement='inspectiontype@workshop';
		include DOL_DOCUMENT_ROOT.'/core/extrafieldsinimport.inc.php';
		$this->import_fieldshidden_array[$r] = array('extra.fk_object' => 'lastrowid-'.MAIN_DB_PREFIX.'workshop_inspectiontype');
		$this->import_regex_array[$r] = array();
		$this->import_examplevalues_array[$r] = array_merge($import_sample, $import_extrafield_sample);
		$this->import_updatekeys_array[$r] = array('t.ref' => 'Ref');
		$this->import_convertvalue_array[$r] = array(
			't.ref' => array(
				'rule'=>'getrefifauto',
				'class'=>(!getDolGlobalString('WORKSHOP_MYOBJECT_ADDON') ? 'mod_inspectiontype_standard' : getDolGlobalString('WORKSHOP_MYOBJECT_ADDON')),
				'path'=>"/core/modules/commande/".(!getDolGlobalString('WORKSHOP_MYOBJECT_ADDON') ? 'mod_inspectiontype_standard' : getDolGlobalString('WORKSHOP_MYOBJECT_ADDON')).'.php'
				'classobject'=>'InspectionType',
				'pathobject'=>'/workshop/class/inspectiontype.class.php',
			),
			't.fk_soc' => array('rule' => 'fetchidfromref', 'file' => '/societe/class/societe.class.php', 'class' => 'Societe', 'method' => 'fetch', 'element' => 'ThirdParty'),
			't.fk_user_valid' => array('rule' => 'fetchidfromref', 'file' => '/user/class/user.class.php', 'class' => 'User', 'method' => 'fetch', 'element' => 'user'),
			't.fk_mode_reglement' => array('rule' => 'fetchidfromcodeorlabel', 'file' => '/compta/paiement/class/cpaiement.class.php', 'class' => 'Cpaiement', 'method' => 'fetch', 'element' => 'cpayment'),
		);
		$this->import_run_sql_after_array[$r] = array();
		$r++; */
		/* END MODULEBUILDER IMPORT MYOBJECT */
	}

	/**
	 *  Function called when module is enabled.
	 *  The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
	 *  It also creates data directories
	 *
	 *  @param      string  $options    Options when enabling module ('', 'noboxes')
	 *  @return     int             	1 if OK, 0 if KO
	 */
	public function init($options = '')
	{
		global $conf, $langs;

		$hookmanager =  new HookManager($db);

		$my_object = $conf->workshop;

		//$result = $this->_load_tables('/install/mysql/', 'workshop');
		$result = $this->_load_tables('/workshop/sql/');
		if ($result < 0) {
			return -1; // Do not activate module if error 'not allowed' returned when loading module SQL queries (the _load_table run sql with run_sql with the error allowed parameter set to 'default')
		}

		// Create extrafields during init
		//include_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
		//$extrafields = new ExtraFields($this->db);
		//$result0=$extrafields->addExtraField('workshop_separator1', "Separator 1", 'separator', 1,  0, 'thirdparty',   0, 0, '', array('options'=>array(1=>1)), 1, '', 1, 0, '', '', 'workshop@workshop', 'isModEnabled("workshop")');
		//$result1=$extrafields->addExtraField('workshop_myattr1', "New Attr 1 label", 'boolean', 1,  3, 'thirdparty',   0, 0, '', '', 1, '', -1, 0, '', '', 'workshop@workshop', 'isModEnabled("workshop")');
		//$result2=$extrafields->addExtraField('workshop_myattr2', "New Attr 2 label", 'varchar', 1, 10, 'project',      0, 0, '', '', 1, '', -1, 0, '', '', 'workshop@workshop', 'isModEnabled("workshop")');
		//$result3=$extrafields->addExtraField('workshop_myattr3', "New Attr 3 label", 'varchar', 1, 10, 'bank_account', 0, 0, '', '', 1, '', -1, 0, '', '', 'workshop@workshop', 'isModEnabled("workshop")');
		//$result4=$extrafields->addExtraField('workshop_myattr4', "New Attr 4 label", 'select',  1,  3, 'thirdparty',   0, 1, '', array('options'=>array('code1'=>'Val1','code2'=>'Val2','code3'=>'Val3')), 1,'', -1, 0, '', '', 'workshop@workshop', 'isModEnabled("workshop")');
		//$result5=$extrafields->addExtraField('workshop_myattr5', "New Attr 5 label", 'text',    1, 10, 'user',         0, 0, '', '', 1, '', -1, 0, '', '', 'workshop@workshop', 'isModEnabled("workshop")');

		// Permissions
		$this->remove($options);

		$sql = array();

		// Document templates
		$moduledir = dol_sanitizeFileName('workshop');
		$myTmpObjects = array();
		$myTmpObjects['InspectionSheet'] = array('includerefgeneration'=>0, 'includedocgeneration'=>0);

		foreach ($myTmpObjects as $myTmpObjectKey => $myTmpObjectArray) {
			if ($myTmpObjectKey == 'InspectionSheet') {
				continue;
			}
			if ($myTmpObjectArray['includerefgeneration']) {
				$src = DOL_DOCUMENT_ROOT.'/install/doctemplates/'.$moduledir.'/template_InspectionSheet.odt';
				$dirodt = DOL_DATA_ROOT.'/doctemplates/'.$moduledir;
				$dest = $dirodt.'/template_InspectionSheet.odt';

				if (file_exists($src) && !file_exists($dest)) {
					require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
					dol_mkdir($dirodt);
					$result = dol_copy($src, $dest, 0, 0);
					if ($result < 0) {
						$langs->load("errors");
						$this->error = $langs->trans('ErrorFailToCopyFile', $src, $dest);
						return 0;
					}
				}

				$sql = array_merge($sql, array(
					"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom = 'standard_".strtolower($myTmpObjectKey)."' AND type = '".$this->db->escape(strtolower($myTmpObjectKey))."' AND entity = ".((int) $conf->entity),
					"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity) VALUES('standard_".strtolower($myTmpObjectKey)."', '".$this->db->escape(strtolower($myTmpObjectKey))."', ".((int) $conf->entity).")",
					"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom = 'generic_".strtolower($myTmpObjectKey)."_odt' AND type = '".$this->db->escape(strtolower($myTmpObjectKey))."' AND entity = ".((int) $conf->entity),
					"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity) VALUES('generic_".strtolower($myTmpObjectKey)."_odt', '".$this->db->escape(strtolower($myTmpObjectKey))."', ".((int) $conf->entity).")"
				));
			}
		}

		return $this->_init($sql, $options);
	}

	/**
	 *  Function called when module is disabled.
	 *  Remove from database constants, boxes and permissions from Dolibarr database.
	 *  Data directories are not deleted
	 *
	 *  @param      string	$options    Options when enabling module ('', 'noboxes')
	 *  @return     int                 1 if OK, 0 if KO
	 */
	public function remove($options = '')
	{
		$sql = array();
		return $this->_remove($sql, $options);
	}
}
