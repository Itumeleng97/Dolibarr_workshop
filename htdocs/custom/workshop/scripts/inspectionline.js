

$(document).ready(function() {
    $('#submitHeaderForm').click(function() {

        // Serialize the form data
        var formData = $('#hearderform').serialize();

        // Send AJAX request
        $.ajax({
            type: "POST",
            url: "<?php echo DOL_URL_ROOT; ?>/custom/workshop/inspectionsheet_card.php?id=<?php echo $id; ?>&action=addlines",
            data: formData,
            success: function(response) {
                // Handle success response
                console.log(response);
                // Show the next form or perform other actions
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
                // Handle errors here
            }
        });
    });
});